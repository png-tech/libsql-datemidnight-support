package net.sf.jparts.libsql.loader;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

public class DefaultQueryLoaderTest {

    @Test
    public void testConstructor() {
        DefaultQueryLoader l = new DefaultQueryLoader();
        Assert.assertTrue(l.cacheEnabled);
        Assert.assertEquals(60, l.initialCapacity);
        Assert.assertTrue(0.75f == l.loadFactor);
        Assert.assertEquals(16, l.concurrencyLevel);
        Assert.assertNotNull(l.queryCache);
        Assert.assertTrue(l.isCacheEnabled());
        Assert.assertEquals("UTF-8", l.fileEncoding);
    }

    @Test
    public void testEncoding() {
        // init loader with UTF-16 file encoding
        DefaultQueryLoader l = new DefaultQueryLoader() {
            @Override
            protected void initEncoding(Properties properties) {
                Properties testProperties = new Properties();
                if (properties != null) {
                    testProperties.putAll(properties);
                }
                testProperties.setProperty("encoding", "UTF-16");

                super.initEncoding(testProperties);
            }
        };

        Assert.assertEquals("UTF-16", l.fileEncoding);
    }

    @Test
    public void testCache1() {
        // init loader with disabled cache
        DefaultQueryLoader l = new DefaultQueryLoader() {
            @Override
            protected void initCacheConfiguration(Properties properties) {
                Properties testProperties = new Properties();
                if (properties != null) {
                    testProperties.putAll(properties);
                }
                testProperties.setProperty(CACHE_ENABLED_KEY, "false");
                testProperties.setProperty(CACHE_LOAD_FACTOR_KEY, "0.5");
                testProperties.setProperty(CACHE_CONCURRENCY_LEVEL_KEY, "10");
                testProperties.setProperty(CACHE_INITIAL_CAPACITY_KEY, "66");

                super.initCacheConfiguration(testProperties);
            }
        };

        Assert.assertFalse(l.cacheEnabled);
        Assert.assertEquals(66, l.initialCapacity);
        Assert.assertTrue(0.5f == l.loadFactor);
        Assert.assertEquals(10, l.concurrencyLevel);
        Assert.assertNull(l.queryCache);
        Assert.assertFalse(l.isCacheEnabled());

        // clear cache, must not fail even if cache is disabled
        l.clearCache();

        try {
            l.preloadQueries(null);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            l.preloadQueries(Arrays.<String>asList());
            Assert.fail("IllegalStateException expected");
        } catch (IllegalStateException ex) {
            // it's ok
        }

        // enable cache
        l.setCacheEnabled(true);
        Assert.assertTrue(l.cacheEnabled);
        Assert.assertNotNull(l.queryCache);
        Assert.assertEquals(0, l.queryCache.size());
        Assert.assertTrue(l.isCacheEnabled());
    }

    @Test
    public void testCache2() {
        // create loader, cache is enabled
        DefaultQueryLoader l = new DefaultQueryLoader();

        // preload queries: existing, non-existing, illegal (null, "")
        Map<String, QueryLoadException> res = l.preloadQueries(
                Arrays.<String>asList("findAll", "findAllUsers", null, "", "qweqwe"));
        Assert.assertEquals(2, l.queryCache.size());
        Assert.assertNotNull(res);
        Assert.assertEquals(1, res.size());
        // noinspection ThrowableResultOfMethodCallIgnored
        Assert.assertNotNull(res.get("qweqwe"));

        // remove illegal queries
        try {
            l.removeFromCache("");
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            l.removeFromCache(null);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        // remove cached query
        l.removeFromCache("findAll");
        Assert.assertEquals(1, l.queryCache.size());

        // load cached query
        String q = l.load("findAllUsers");
        Assert.assertNotNull(q);

        l.clearCache();
        Assert.assertEquals(0, l.queryCache.size());
    }

    @Test
    public void testLoad() {
        DefaultQueryLoader l = new DefaultQueryLoader();

        final String unexpectedException = "Unexpected exception caught";
        final String correctFindAllSql = "select * from t1";

        // invalid argument case
        try {
            l.load(null);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        } catch (Exception ex) {
            Assert.fail(unexpectedException + ex.getClass().getName());
        }

        // query does not exists
        try {
            l.load("qqqwwweeerrr");
            Assert.fail("QueryLoadException expected");
        } catch (QueryLoadException ex) {
            // it's ok
        } catch (Exception ex) {
            Assert.fail(unexpectedException + ex.getClass().getName());
        }

        // empty query
        try {
            l.load("empty");
            Assert.fail("QueryLoadException expected");
        } catch (QueryLoadException ex) {
            // it's ok
        } catch (Exception ex) {
            Assert.fail(unexpectedException + ex.getClass().getName());
        }

        // correct query
        try {
            String sql = l.load("findAll");
            Assert.assertNotNull(sql);
            Assert.assertEquals(sql, correctFindAllSql);
        } catch (Exception ex) {
            Assert.fail(unexpectedException + ex.getClass().getName());
        }
    }
}
