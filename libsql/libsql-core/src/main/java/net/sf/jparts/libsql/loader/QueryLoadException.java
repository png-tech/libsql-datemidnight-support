package net.sf.jparts.libsql.loader;

/**
 * Thrown when {@link QueryLoader} can not load query string in cause of runtime errors
 * or if query string is empty.
 */
public class QueryLoadException extends RuntimeException {

    public QueryLoadException() {
    }

    public QueryLoadException(String message) {
        super(message);
    }

    public QueryLoadException(String message, Throwable cause) {
        super(message, cause);
    }

    public QueryLoadException(Throwable cause) {
        super(cause);
    }
}
