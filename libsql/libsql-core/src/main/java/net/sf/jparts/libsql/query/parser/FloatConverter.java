package net.sf.jparts.libsql.query.parser;

public final class FloatConverter extends AbstractDecimalConverter<Float> {

    public FloatConverter() {
    }

    public FloatConverter(boolean safe, Float errorValue, Float nullValue) {
        super(safe, errorValue, nullValue);
    }

    @Override
    protected Float tryCreate(String value) throws NumberFormatException {
        return Float.valueOf(value);
    }
}
