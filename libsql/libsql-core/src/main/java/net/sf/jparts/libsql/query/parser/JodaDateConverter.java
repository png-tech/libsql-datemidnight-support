package net.sf.jparts.libsql.query.parser;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

/**
 * Convert string to Joda DateTime instance with time 00:00:00.000.
 */
public class JodaDateConverter extends AbstractJodaDateConverter<DateTime> {

    public static final String DEFAULT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    public JodaDateConverter() {
        super(DEFAULT_PATTERN);
    }

    public JodaDateConverter(String pattern) {
        super(pattern);
    }

    public JodaDateConverter(boolean safe, String pattern, DateTime errorValue, DateTime nullValue) {
        super(safe, pattern, errorValue, nullValue);
    }

    @Override
    protected DateTime tryParse(String value, DateTimeFormatter format) {
        return format.parseDateTime(value).withMillisOfDay(0);
    }
}
