package net.sf.jparts.libsql.mapping.builder;

import net.sf.jparts.libsql.mapping.*;
import net.sf.jparts.libsql.mapping.types.StringTypeHandler;
import org.junit.Assert;
import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class XmlMappingBuilderTest {

    private XmlMappingBuilder builder = new XmlMappingBuilder();

    @Test
    public void testBuild_xsd_test1() {
        try {
            builder.build("xsd-test1-correct");
        } catch (XmlParsingException ex) {
            Assert.assertThat(ex.getMessage(), containsString("result and column"));
        }
    }

    @Test
    public void testBuild_correct_mapping1() {
        ResultMapping mapping = builder.build("correct-mapping1");

        assertNotNull(mapping);
        assertEquals("correct-mapping1", mapping.getId());
        assertEquals(Object.class, mapping.getType());
        assertFalse(mapping.isAutoMapping());

        // constructor
        assertNotNull(mapping.getConstructorMapping());
        assertNotNull(mapping.getConstructorMapping().getArguments());
        assertEquals(2, mapping.getConstructorMapping().getArguments().size());

        // constructor arguments
        // arg1
        ArgumentMapping a1 = mapping.getConstructorMapping().getArguments().get(0);
        assertEquals(String.class, a1.getType());
        assertTrue(a1 instanceof SimpleArgumentMapping);
        SimpleArgumentMapping s1 = (SimpleArgumentMapping) a1;
        assertEquals("arg_column", s1.getColumn());
        assertNotNull(s1.getTypeHandler());
        assertEquals(StringTypeHandler.class, s1.getTypeHandler().getClass());

        // arg2
        ArgumentMapping a2 = mapping.getConstructorMapping().getArguments().get(1);
        assertEquals(String.class, a2.getType());
        assertTrue(a2 instanceof SimpleArgumentMapping);
        SimpleArgumentMapping s2 = (SimpleArgumentMapping) a2;
        assertEquals("arg_column", s2.getColumn());
        assertNotNull(s2.getTypeHandler());
        assertEquals(StringTypeHandler.class, s2.getTypeHandler().getClass());

        // discriminator
        DiscriminatorMapping dm = mapping.getDiscriminatorMapping();
        assertNotNull(dm);
        assertEquals("disc_column", dm.getColumn());

        assertNotNull(dm.getMappings());
        ResultMapping d1 = dm.getMappings().get("disc_v1");
        assertNotNull(d1);
        assertEquals("nested-stub1", d1.getId());

        ResultMapping d2 = dm.getMappings().get("disc_v2");
        assertNotNull(d2);
        assertEquals("nested-stub2", d2.getId());

        // properties
        assertNotNull(mapping.getPropertyMappings());

        // 1
        PropertyMapping p1 = mapping.getPropertyMappings().get("prop1");
        assertNotNull(p1);
        assertEquals("prop1", p1.getName());
        assertTrue(p1 instanceof SimplePropertyMapping);
        SimplePropertyMapping sp1 = (SimplePropertyMapping) p1;
        assertEquals("p1_column", sp1.getColumn());
        assertEquals(StringTypeHandler.class, sp1.getTypeHandler().getClass());

        // 2
        PropertyMapping p2 = mapping.getPropertyMappings().get("prop2");
        assertNotNull(p2);
        assertEquals("prop2", p2.getName());
        assertTrue(p2 instanceof SimplePropertyMapping);
        SimplePropertyMapping sp2 = (SimplePropertyMapping) p2;
        assertEquals("p2_column", sp2.getColumn());
        assertEquals(StringTypeHandler.class, sp2.getTypeHandler().getClass());

        // 3: association
        PropertyMapping p3 = mapping.getPropertyMappings().get("prop3");
        assertNotNull(p3);
        assertEquals("prop3", p3.getName());
        assertTrue(p3 instanceof AssociationMapping);

        AssociationMapping ap3 = (AssociationMapping) p3;
        assertEquals("p3_prefix", ap3.getColumnPrefix());
        ResultMapping ar = ap3.getNested();
        assertNotNull(ar);
        assertEquals("nested-stub3", ar.getId());
    }

    @Test
    public void testBuild_incorrect_mapping1() {
        try {
            builder.build("incorrect-mapping1");
        } catch (XmlParsingException ex) {
            Assert.assertThat(ex.getMessage(), containsString("-qqqq"));
        }
    }

    @Test
    public void testBuild_incorrect_mapping2() {
        try {
            builder.build("incorrect-mapping2");
        } catch (XmlParsingException ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            Assert.assertThat(sw.toString(), containsString("disc_v1"));
        }
    }

}
