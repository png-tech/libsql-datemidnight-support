package net.sf.jparts.libsql.mapping.model.test5;

public class E1 {

    private String a;

    private String c;

    public String getA() {
        return a;
    }

    public String getC() {
        return c;
    }

    @Override
    public String toString() {
        return "E1{" +
                "a='" + a + '\'' +
                ", c='" + c + '\'' +
                '}';
    }
}
