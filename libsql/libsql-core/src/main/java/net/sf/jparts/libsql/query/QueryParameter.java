package net.sf.jparts.libsql.query;

/**
 * Type for query parameter objects.
 * @param <T> the type of the parameter
 */
public interface QueryParameter<T> {

    /**
     * Return the parameter name, or <code>null</code> if the parameter is not a
     * named parameter or no name has been assigned.
     *
     * @return parameter name
     */
    public String getName();

    /**
     * Return the Java type of the parameter. Values bound to the parameter must be
     * assignable to this type. This method is required to be supported for select
     * queries only.
     *
     * @return the Java type of the parameter
     * @throws IllegalStateException if invoked on a parameter when the implementation does not support this use
     */
    public Class<T> getParameterType();

    /**
     * Return the parameter position, or null if the parameter is not a positional parameter.
     *
     * @return position of parameter
     */
    public Integer getPosition();
}
