package net.sf.jparts.libsql;

import net.sf.jparts.libsql.common.Configuration;
import net.sf.jparts.libsql.finder.Finder;
import net.sf.jparts.libsql.finder.FinderNotFoundException;
import net.sf.jparts.libsql.finder.FinderRegistry;
import net.sf.jparts.libsql.finder.FinderResult;
import net.sf.jparts.libsql.loader.QueryLoader;
import net.sf.jparts.libsql.loader.QueryLoaderService;
import net.sf.jparts.libsql.query.SelectQuery;
import net.sf.jparts.libsql.query.SelectQueryImpl;

import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Facade to LibSql library services.
 *
 * <p>
 * Of course you may use {@link QueryLoader}, {@link FinderRegistry} and {@link Finder} by itself.
 * But this facade provides more simple API in typical cases.
 * <p>
 * Simple example:
 * <pre>
 * SelectQuery&lt;UserEntity&gt; query = LibSql
 *         .makeSelectQuery("findAllUsers", UserEntity.class)
 *         .orderBy(Order.desc("name"))
 *         .setMaxResults(10);
 * FinderResult&lt;UserEntity&gt; r = LibSql.find(query);
 * </pre>
 */
public class LibSql {

    /**
     * Name for queries created by {@link #makeSelectQueryByString(String, Class)} method.
     */
    public static final String BY_QUERY_STRING_QUERY_NAME = "net.sf.jparts.libsql.byQueryString";

    /**
     * This default finder name will be used if it is not defined in configuration.
     */
    public static final String DEFAULT_DEFAULT_FINDER_NAME = "default";

    /**
     * Defines default finder name configuration property.
     */
    public static final String DEFAULT_FINDER_NAME_PROPERTY = "defaultFinderName";

    /**
     * Default finder name value.
     */
    private static final String defaultFinderName;

    private static final Logger logger = LoggerFactory.getLogger(LibSql.class);

    static {
        Properties p = Configuration.get(LibSql.class);
        String dfn = null;
        if (p != null) {
            dfn = p.getProperty(DEFAULT_FINDER_NAME_PROPERTY);
        }

        if (dfn == null || dfn.trim().length() < 1) {
            dfn = DEFAULT_DEFAULT_FINDER_NAME;
        } else {
            dfn = dfn.trim();
        }
        
        defaultFinderName = dfn;
    }

    /**
     * Returns default finder name. It is error in library configuration if finder with this name
     * is not present in {@link FinderRegistry}.
     *
     * @return non-null non-empty string.
     */
    public static String getDefaultFinderName() {
        return defaultFinderName;
    }

    /**
     * Execute given <code>query</code> against default finder. See {@link #find(String, SelectQuery)} description
     * for more information.
     *
     * @param query query to execute.
     * @param <E> class of objects in result list.
     *
     * @return a {@link FinderResult} instance, not <code>null</code>.
     *
     * @throws FinderNotFoundException if default finder does not exists in {@link FinderRegistry}.
     */
    public static <E> FinderResult<E> find(SelectQuery<E> query) {
        return find(getDefaultFinderName(), query);
    }

    /**
     * Execute given <code>query</code> against default finder. See {@link #find(String, SelectQuery)} description
     * for more information. Useful for queries without paging.
     *
     * @param query query to execute.
     * @param <E> class of objects in result list.
     *
     * @return a {@link List} instance, not <code>null</code>.
     *
     * @throws FinderNotFoundException if default finder does not exists in {@link FinderRegistry}.
     *
     * @since 2.0.0
     *
     * @see net.sf.jparts.libsql.finder.FinderResult#getData()
     */
    public static <E> List<E> findList(SelectQuery<E> query) {
        return find(query).getData();
    }

    /**
     * Execute given <code>query</code> against default finder and returns single result.
     * Useful for queries without paging.
     *
     * @param query query to execute.
     * @param <E> class of objects in result list.
     *
     * @return an instance of {@code E}, maybe {@code null}.
     *
     * @throws FinderNotFoundException if default finder does not exists in {@link FinderRegistry}.
     * @throws net.sf.jparts.libsql.finder.IncorrectResultSizeException if there is no rows or more
     *      than one row in result.
     *
     * @since 2.0.0
     *
     * @see net.sf.jparts.libsql.finder.FinderResult#getSingleResult()
     */
    public static <E> E findSingle(SelectQuery<E> query) {
        return find(query).getSingleResult();
    }

    /**
     * Execute <code>query</code> against specified finder. {@link Finder} will be obtained from
     * {@link FinderRegistry} by given <code>finderName</code>. {@link QueryLoader} will be executed
     * automatically if <code>query</code> class is default {@link SelectQuery} implemetation
     * (it may be created by {@link #makeSelectQuery(String, Class)}) and query string is not loaded yet.
     *
     * @param finderName finder name.
     * @param query query to execute.
     * @param <E> class of objects in result list.
     *
     * @return a {@link FinderResult} instance, not <code>null</code>.
     *
     * @throws IllegalArgumentException if <code>finderName</code> is empty or <code>null</code>,
     *  or <code>query</code> is <code>null</code>. 
     * @throws FinderNotFoundException if finder with given name does not
     *  exists in {@link FinderRegistry}.
     */
    public static <E> FinderResult<E> find(String finderName, SelectQuery<E> query) {
        if (query == null) {
            throw new IllegalArgumentException("query must not be null");
        }

        // lazy loading
        if ((query.getQueryString() == null || query.getQueryString().trim().length() < 1)
                && (query instanceof SelectQueryImpl)) {

            // get loader and load query string
            QueryLoader loader = QueryLoaderService.getLoader();
            String queryString = loader.load(query.getName());

            // set query string
            SelectQueryImpl<E> sq = (SelectQueryImpl<E>) query;
            sq.setQueryString(queryString);
        }

        logQuery(finderName, query);

        // get finder from registry and invoke query
        Finder f = FinderRegistry.getInstance().get(finderName);
        FinderResult<E> r = f.find(query);
        return r;
    }

    /**
     * See {@link #find(String, net.sf.jparts.libsql.query.SelectQuery)} and
     * {@link net.sf.jparts.libsql.finder.FinderResult#getData()}. Useful for queries without paging.
     *
     * @return a {@link List} instance, not {@code null}.
     *
     * @throws IllegalArgumentException if <code>finderName</code> is empty or {@code null},
     *  or <code>query</code> is {@code null}.
     * @throws FinderNotFoundException if finder with given name does not
     *  exists in {@link FinderRegistry}.
     *
     * @since 2.0.0
     * @see #findList(net.sf.jparts.libsql.query.SelectQuery)
     * @see #find(String, net.sf.jparts.libsql.query.SelectQuery)
     * @see net.sf.jparts.libsql.finder.FinderResult#getData()
     */
    public static <E> List<E> findList(String finderName, SelectQuery<E> query) {
        return find(finderName, query).getData();
    }

    /**
     * @since 2.0.0
     *
     * @see #find(String, net.sf.jparts.libsql.query.SelectQuery)
     * @see #findSingle(net.sf.jparts.libsql.query.SelectQuery)
     */
    public static <E> E findSingle(String finderName, SelectQuery<E> query) {
        return find(finderName, query).getSingleResult();
    }

    /**
     * Write query data to debug log.
     *
     * @param finderName finder name.
     * @param query  query to execute.
     */
    private static <E> void logQuery(String finderName, SelectQuery<E> query) {
        logger.debug("LibSql#find({}, {})", finderName, query);
    }

    /**
     * Creates {@link SelectQuery} instance by given class and query name.
     *
     * @param queryName name of query.
     * @param resultClass class of objects in result list.
     * @param <E> class of objects in result list.
     *
     * @return a {@link SelectQuery} instance, default implementation class will be used.
     *
     * @throws IllegalArgumentException if <code>queryName</code> is empty or <code>null</code>
     *  or if <code>resultClass</code> is <code>null</code>.
     */
    public static <E> SelectQuery<E> makeSelectQuery(String queryName, Class<E> resultClass) {
        if (queryName == null || queryName.trim().length() < 1) {
            throw new IllegalArgumentException("queryName must not be empty or null");
        }
        if (resultClass == null) {
            throw new IllegalArgumentException("resultClass must not be null");
        }

        SelectQueryImpl<E> sq = new SelectQueryImpl<E>(queryName);
        sq.setResultClass(resultClass);
        return sq;
    }

    /**
     * Creates {@link SelectQuery} instance by given class and query string. {@link #find(SelectQuery)}
     * and {@link #find(String, SelectQuery)} methods will not perform lazy-loading for query created in
     * this way (because query string already set). Query name will be set to {@link #BY_QUERY_STRING_QUERY_NAME} value.
     *
     * @param queryString query string.
     * @param resultClass class of objects in result list.
     * @param <E> class of objects in result list.
     *
     * @return a {@link SelectQuery} instance, default implementation class will be used.
     *
     * @throws IllegalArgumentException if <code>queryString</code> is empty or <code>null</code>
     *  or if <code>resultClass</code> is <code>null</code>.
     */
    public static <E> SelectQuery<E> makeSelectQueryByString(String queryString, Class<E> resultClass) {
        if (queryString == null || queryString.trim().length() < 1) {
            throw new IllegalArgumentException("queryString must not be empty or null");
        }
        if (resultClass == null) {
            throw new IllegalArgumentException("resultClass must not be null");
        }

        SelectQueryImpl<E> sq = new SelectQueryImpl<E>(BY_QUERY_STRING_QUERY_NAME, queryString);
        sq.setResultClass(resultClass);
        return sq;
    }
}
