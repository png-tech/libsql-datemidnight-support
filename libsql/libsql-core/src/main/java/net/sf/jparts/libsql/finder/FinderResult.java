package net.sf.jparts.libsql.finder;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "finderResult", namespace = "http://jparts.sf.net/libsql/finder")
@XmlAccessorType(XmlAccessType.FIELD)
public class FinderResult<E> implements Serializable {

    private static final long serialVersionUID = -5671858865970448085L;

    public static final int NO_TOTAL_LENGTH = -1;

    private int offset;

    private int totalLength;

    @XmlElementWrapper(name = "dataList")
    @XmlElement(name = "data")
    private List<E> data;

    public FinderResult() {
    }

    public FinderResult(int offset, int totalLength, List<E> data) {
        this.offset = offset;
        this.totalLength = totalLength;
        this.data = data;
    }

    public int getTotalLength() {
        return totalLength;
    }

    public boolean isEmpty() {
        return data != null && data.isEmpty();
    }

    public int size() {
        return (data == null) ? 0 : data.size();
    }

    /**
     * Returns underlying data list or empty list if result is empty. Changes in the returned collection
     * reflected to FinderResult if it is not empty. Method behavior changed in version 2.0.0.
     * @return not {@code null}.
     */
    public List<E> getData() {
        return (data != null) ? data : Collections.<E>emptyList();
    }

    /**
     * Returns copy of underlying data list or empty list if result is empty. Changes in returned collection
     * are not reflected to FinderResult.
     * @return not {@code null}.
     *
     * @since 2.0.0
     */
    public List<E> getDataCopy() {
        return (data != null) ? new ArrayList<>(data) : Collections.<E>emptyList();
    }

    /**
     * Returns the first record, it doesn't matter how many records in result, if result is empty returns {@code null}.
     * @return maybe {@code null}
     */
    public E getFirstResult() {
        if (data != null && data.size() > 0) {
            return data.get(0);
        }
        
        return null;
    }

    /**
     * Returns the first record if it is the only, throws exception otherwise.
     * @return maybe {@code null} if first result is null.
     *
     * @throws IncorrectResultSizeException if there is no rows or more than one row in result
     *
     * @since 2.0.0
     */
    public E getSingleResult() {
        if (data == null || data.size() == 0) {
            throw new IncorrectResultSizeException(1, 0);
        }

        if (data.size() > 1) {
            throw new IncorrectResultSizeException(1, data.size());
        }

        return data.get(0);
    }

    public int getOffset() {
        return offset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinderResult<?> that = (FinderResult) o;

        if (offset != that.offset) return false;
        if (totalLength != that.totalLength) return false;
        //noinspection RedundantIfStatement
        if (data != null ? !data.equals(that.data) : that.data != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = offset;
        result = 31 * result + totalLength;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }
}
