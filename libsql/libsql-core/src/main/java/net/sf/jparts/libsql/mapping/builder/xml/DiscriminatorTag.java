package net.sf.jparts.libsql.mapping.builder.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiscriminatorMapping", propOrder = {
    "cases"
})
public class DiscriminatorTag {

    @XmlElement(name = "case", required = true)
    public List<CaseTag> cases;

    @XmlAttribute(name = "column", required = true)
    public String column;

}
