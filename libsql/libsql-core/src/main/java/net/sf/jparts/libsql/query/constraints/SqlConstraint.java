package net.sf.jparts.libsql.query.constraints;

public class SqlConstraint implements Constraint {

    private final String sql;

    protected SqlConstraint(String sql) {
        if (sql == null || sql.isEmpty()) {
            throw new IllegalArgumentException("sql must not be empty string or null");
        }
        this.sql = sql;
    }

    @Override
    public String toString() {
        return sql;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SqlConstraint that = (SqlConstraint) o;

        //noinspection RedundantIfStatement
        if (!sql.equals(that.sql)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return sql.hashCode();
    }
}
