package net.sf.jparts.libsql.query.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ShortConverter implements ValueConverter<Short> {

    public static final int DEFAULT_RADIX = 10;

    private Logger logger = LoggerFactory.getLogger(ValueConverter.LOGGER_NAME);

    private final Short errorValue;

    private final Short nullValue;

    private final boolean safe;

    private final int radix;

    public ShortConverter() {
        this(true, null, null, DEFAULT_RADIX);
    }

    public ShortConverter(boolean safe, Short errorValue, Short nullValue, int radix) {
        this.safe = safe;
        this.errorValue = errorValue;
        this.nullValue = nullValue;
        this.radix = radix;
    }

    @Override
    public Short convert(String value) {
        if (value == null) return nullValue;

        try {
            return Short.valueOf(value, radix);
        } catch (NumberFormatException ex) {
            if (safe) {
                logger.debug("Can't convert, value = {}", value, ex);
                return errorValue;
            }
            throw new ValueFormatException("Can't convert, value = " + value, ex);
        }
    }
}
