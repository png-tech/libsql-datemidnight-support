package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "isNull")
@XmlAccessorType(XmlAccessType.FIELD)
public class NullConstraint implements Constraint {

    private static final long serialVersionUID = -6514735885255040618L;

    @XmlValue
    private String property;

    public NullConstraint() {
    }

    public NullConstraint(String property) {
        this.property = property;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NullConstraint that = (NullConstraint) o;

        if (property != null ? !property.equals(that.property) : that.property != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return property != null ? property.hashCode() : 0;
    }
}
