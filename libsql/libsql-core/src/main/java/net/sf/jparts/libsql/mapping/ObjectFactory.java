package net.sf.jparts.libsql.mapping;

/**
 * Creates new object instances.
 */
public interface ObjectFactory {

    /**
     * Create new instance of specified class with default constructor.
     *
     * @param clazz object class.
     * @param <T> generic type.
     * @return not <code>null</code>.
     *
     * @throws ObjectCreationException in case of any problems.
     */
    public <T> T create(Class<T> clazz);

    /**
     * Create new instance of specified class via constructor with given arguments types and values.
     *
     * @param clazz object class.
     * @param types constructor argument types.
     * @param values constructor argument values.
     * @param <T> generic type.
     * @return not <code>null</code>.
     *
     * @throws ObjectCreationException in case of any problems.
     */
    public <T> T create(Class<T> clazz, Class<?>[] types, Object[] values);
}
