package net.sf.jparts.libsql.spring.template;

import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.*;

public class AdvancedJdbcTemplate extends JdbcTemplate {

    private final QueryModifier modifier;

    public static Builder builder(DataSource dataSource) {
        return new Builder(dataSource);
    }

    private AdvancedJdbcTemplate(Builder b) {
        super(b.ds, b.lazyInit);
        this.modifier = b.modifier;
    }

    protected String processQuery(String sql) {
        return (modifier == null) ? sql : modifier.apply(sql);
    }

    // overriden methods

    @Override
    public void execute(final String sql) throws DataAccessException {
        super.execute(processQuery(sql));
    }

    @Override
    public <T> T query(final String sql, final ResultSetExtractor<T> rse) throws DataAccessException {
        return super.query(processQuery(sql), rse);
    }

    @Override
    public int update(String sql, PreparedStatementSetter pss) throws DataAccessException {
        return super.update(processQuery(sql), pss);
    }

    @Override
    public int[] batchUpdate(String sql, final BatchPreparedStatementSetter pss) throws DataAccessException {
        return super.batchUpdate(processQuery(sql), pss);
    }

    @Override
    public int[] batchUpdate(String sql, List<Object[]> batchArgs, int[] argTypes) {
        return super.batchUpdate(processQuery(sql), batchArgs, argTypes);
    }

    @Override
    public <T> int[][] batchUpdate(String sql, final Collection<T> batchArgs, final int batchSize, final ParameterizedPreparedStatementSetter<T> pss) {
        return super.batchUpdate(processQuery(sql), batchArgs, batchSize, pss);
    }

    @Override
    public <T> T execute(String callString, CallableStatementCallback<T> action) throws DataAccessException {
        return super.execute(processQuery(callString), action);
    }

    @Override
    public <T> T query(String sql, PreparedStatementSetter pss, ResultSetExtractor<T> rse) throws DataAccessException {
        return super.query(processQuery(sql), pss, rse);
    }

    // builder

    public static class Builder {
        private DataSource ds;
        private boolean lazyInit = true;
        private QueryModifier modifier;

        public Builder(DataSource ds) {
            this.ds = requireNonNull(ds, "ds must not be null");
        }

        public Builder setLazyInit(boolean lazyInit) {
            this.lazyInit = lazyInit;
            return this;
        }

        public Builder withModifier(QueryModifier modifier) {
            this.modifier = modifier;
            return this;
        }

        public AdvancedJdbcTemplate build() {
            return new AdvancedJdbcTemplate(this);
        }

        private <T> T requireNonNull(T value, String message) {
            if (value == null)
                throw new IllegalArgumentException(message);
            return value;
        }
    }
}
