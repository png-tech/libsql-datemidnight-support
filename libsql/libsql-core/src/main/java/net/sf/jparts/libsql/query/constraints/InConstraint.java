package net.sf.jparts.libsql.query.constraints;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InConstraint implements OnePropertyConstraint {

    private final String propertyName;

    private final List<Object> values = new ArrayList<>();

    protected InConstraint(String propertyName, Object... values) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("propertyName must not be empty string or null");
        }
        if (values == null || values.length < 1) {
            throw new IllegalArgumentException("values must not be empty rray or null");
        }
        this.propertyName = propertyName.trim();
        this.values.addAll(Arrays.asList(values));
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }

    public List<Object> getValues() {
        return new ArrayList<>(values);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(propertyName);
        sb.append(" in (");
        if (values.size() > 0) {
            for (int i = 0; i < values.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(values.get(i));
            }
        }
        sb.append(")");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InConstraint that = (InConstraint) o;

        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) return false;
        //noinspection RedundantIfStatement
        if (values != null ? !values.equals(that.values) : that.values != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = propertyName != null ? propertyName.hashCode() : 0;
        result = 31 * result + (values != null ? values.hashCode() : 0);
        return result;
    }
}
