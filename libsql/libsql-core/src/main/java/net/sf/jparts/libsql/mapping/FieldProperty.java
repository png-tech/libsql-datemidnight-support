package net.sf.jparts.libsql.mapping;

import java.lang.reflect.Field;

import net.sf.jparts.libsql.mapping.types.Primitives;

/**
 * {@link ObjectProperty} implementation which works with object fields (from public to private) via reflection.
 */
public class FieldProperty implements ObjectProperty {

    /**
     * Cached reference to field instance.
     */
    protected Field field;

    public FieldProperty(Field field) {
        if (field == null) {
            throw new IllegalArgumentException("field must not be empty or null");
        }
        this.field = field;
        this.field.setAccessible(true);
    }

    @Override
    public Class<?> getType() {
        return field.getType();
    }

    @Override
    public String getName() {
        return field.getName();
    }

    @Override
    public void setValue(Object instance, Object value) {
        try {
            Class<?> type = field.getType();
            Object v = (type.isPrimitive()) ? Primitives.getOrDefault(type, value) : value;
            field.set(instance, v);
        } catch (IllegalAccessException ex) {
            throw new UpdatePropertyException(ex.getMessage(), ex);
        }
    }
}
