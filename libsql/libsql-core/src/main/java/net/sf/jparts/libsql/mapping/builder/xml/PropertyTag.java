package net.sf.jparts.libsql.mapping.builder.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PropertyMapping")
public class PropertyTag {

    @XmlAttribute(name = "name", required = true)
    public String name;

    @XmlAttribute(name = "column")
    public String column;

    @XmlAttribute(name = "typeHandler")
    public String typeHandler;

}
