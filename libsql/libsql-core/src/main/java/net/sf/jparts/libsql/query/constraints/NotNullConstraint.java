package net.sf.jparts.libsql.query.constraints;

public class NotNullConstraint implements OnePropertyConstraint {

    private final String propertyName;

    protected NotNullConstraint(String propertyName) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("propertyName must not be empty string or null");
        }
        this.propertyName = propertyName.trim();
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }

    @Override
    public String toString() {
        return propertyName + " is not null";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotNullConstraint that = (NotNullConstraint) o;

        //noinspection RedundantIfStatement
        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return propertyName != null ? propertyName.hashCode() : 0;
    }
}
