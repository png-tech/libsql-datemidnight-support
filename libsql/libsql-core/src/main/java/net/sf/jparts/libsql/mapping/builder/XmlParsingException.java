package net.sf.jparts.libsql.mapping.builder;

/**
 * Thrown by {@link XmlMappingBuilder} if any error in xml binding file found.
 */
public class XmlParsingException extends RuntimeException {

    public XmlParsingException() {
    }

    public XmlParsingException(String message) {
        super(message);
    }

    public XmlParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public XmlParsingException(Throwable cause) {
        super(cause);
    }

    public XmlParsingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
