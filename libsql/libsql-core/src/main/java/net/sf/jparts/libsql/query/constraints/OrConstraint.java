package net.sf.jparts.libsql.query.constraints;

/**
 * <code>constraint1 or constraint2 or ... constraintN</code>
 */
public class OrConstraint extends AbstractJoinConstraint {

    protected OrConstraint() {
        joinOperator = "or";
        appendSpaces = true;
    }
}
