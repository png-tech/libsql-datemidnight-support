package net.sf.jparts.libsql.query.parser;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract public class AbstractJodaDateConverter<T> implements ValueConverter<T> {

    private Logger logger = LoggerFactory.getLogger(ValueConverter.LOGGER_NAME);

    private final T nullValue;

    private final T errorValue;

    private final DateTimeFormatter formatter;

    private final boolean safe;

    public AbstractJodaDateConverter(String pattern) {
        this(true, pattern, null, null);
    }

    public AbstractJodaDateConverter(boolean safe, final String pattern, T errorValue, T nullValue) {
        this.safe = safe;
        this.formatter = DateTimeFormat.forPattern(pattern);
        this.errorValue = errorValue;
        this.nullValue = nullValue;
    }

    @Override
    public T convert(String value) {
        if (value == null || value.isEmpty())
            return nullValue;

        try {
            return tryParse(value, formatter);
        } catch (IllegalArgumentException ex) {
            if (safe) {
                logger.debug("Can't convert, value = {}", value);
                return errorValue;
            }
            throw new ValueFormatException("Can't convert, value = " + value, ex);
        }
    }

    abstract protected T tryParse(String value, DateTimeFormatter format);
}
