package net.sf.jparts.libsql.query.parser;

/**
 * May be used in {@link QueryParser} implementation for
 * creating correct Java object from string value of some parameter.
 *
 * <p>
 * All {@code ValueConverter} implementations must be thread safe.
 *
 * <p>
 * It's recommended to create class in your project with definition of
 * frequently used converters. Example
<pre>
abstract public class Converters {

    public static final ValueConverter<Integer> INTEGER = new IntegerConverter();

    public static final ValueConverter<Integer> HEX_INTEGER = new IntegerConverter(true, null, null, 16);

    public static final ValueConverter<Long> LONG = new LongConverter();

    public static final ValueConverter<BigDecimal> BIG_DECIMAL = new BigDecimalConverter();

    public static final ValueConverter<Boolean> BOOLEAN = new BooleanConverter();

    public static final ValueConverter<Date> DATE = new SqlDateConverter("yyyy-MM-dd'T'HH:mm:ss");

    public static final ValueConverter<DateTime> JODA_DATE_TIME = new JodaDateTimeConverter();

    private Converters() {
    }
}
</pre>
 *
 * @param <T> result type.
 */
public interface ValueConverter<T> {

    /**
     * Logger name for all converter implementations in library.
     * It's recommended to use it for your own converters too.
     */
    public static final String LOGGER_NAME = "net.sf.jparts.libsql.query.parser.Converters";

    /**
     * Creates Java object from its string representation.
     *
     * @param value may be {@code null} or empty string.
     * @return converted value, may be {@code null}.
     *
     * @throws ValueFormatException if converter does not support input value format.
     */
    public T convert(String value);
}
