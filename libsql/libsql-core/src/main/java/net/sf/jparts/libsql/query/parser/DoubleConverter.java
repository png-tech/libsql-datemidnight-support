package net.sf.jparts.libsql.query.parser;

public final class DoubleConverter extends AbstractDecimalConverter<Double> {

    public DoubleConverter() {
    }

    public DoubleConverter(boolean safe, Double errorValue, Double nullValue) {
        super(safe, errorValue, nullValue);
    }

    @Override
    protected Double tryCreate(String value) throws NumberFormatException {
        return Double.valueOf(value);
    }
}
