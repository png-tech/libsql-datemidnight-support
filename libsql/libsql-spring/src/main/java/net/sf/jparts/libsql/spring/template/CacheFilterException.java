package net.sf.jparts.libsql.spring.template;

import org.springframework.dao.DataAccessException;

public class CacheFilterException extends DataAccessException {

    public CacheFilterException(String msg) {
        super(msg);
    }

    public CacheFilterException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
