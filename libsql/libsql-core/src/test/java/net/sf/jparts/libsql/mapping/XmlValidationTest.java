package net.sf.jparts.libsql.mapping;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class XmlValidationTest {

    private static final String JAXP_SCHEMA_LANGUAGE =
            "http://java.sun.com/xml/jaxp/properties/schemaLanguage";

    private static final String W3C_XML_SCHEMA =
            "http://www.w3.org/2001/XMLSchema";

    private static final String JAXP_SCHEMA_SOURCE =
            "http://java.sun.com/xml/jaxp/properties/schemaSource";

    // dom
    protected Document parse(String xmlFile) throws SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setValidating(true);
        factory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);

        try {
            try (InputStream xsd = open("META-INF/libsql/xsd/libsql-mapping-2.0.xsd");
                    InputStream xml = open("META-INF/libsql/mapping/" + xmlFile + ".xml")) {
                factory.setAttribute(JAXP_SCHEMA_SOURCE, xsd);

                DocumentBuilder builder = factory.newDocumentBuilder();
                builder.setErrorHandler(new ThrowAllErrorHandler());
                InputSource x = new InputSource(new InputStreamReader(xml));
                return builder.parse(x);
            }
        } catch (IOException | ParserConfigurationException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private InputStream open(String file) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(file);
    }

    private String format(Document doc) { // for debug
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            StreamResult result = new StreamResult(new StringWriter());
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            return result.getWriter().toString();
        } catch (TransformerException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @Test
    public void testCorrectData() throws SAXException {
        Document doc = parse("xsd-test1-correct");
        // System.out.println(format(doc)); // debug
        Assert.assertNotNull(doc);
    }

    @Test
    public void testIncorrectData_PropertyNameIsNotUnique() {
        try {
            Document doc = parse("xsd-test2-incorrect-pname");
            // System.out.println(format(doc)); // debug
        } catch (SAXException ex) {
            assertThat(ex.getMessage(), containsString("qwerty"));
        }
    }

    @Test
    public void testIncorrectData_AssociationNameIsNotUnique() throws SAXException {
        try {
            Document doc = parse("xsd-test3-incorrect-aname");
            // System.out.println(format(doc)); // debug
        } catch (SAXException ex) {
            assertThat(ex.getMessage(), containsString("asdfg"));
        }
    }

    @Test
    public void testIncorrectDate_DiscriminatorCaseIsNotUnique() throws SAXException {
        try {
            Document doc = parse("xsd-test4-incorrect-case");
            // System.out.println(format(doc)); // debug
        } catch (SAXException ex) {
            assertThat(ex.getMessage(), containsString("disc_v1"));
        }
    }
}
