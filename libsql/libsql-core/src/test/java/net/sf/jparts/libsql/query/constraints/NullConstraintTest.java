package net.sf.jparts.libsql.query.constraints;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for {@link NullConstraint}.
 */
public class NullConstraintTest {

    private static NullConstraint nc;

    @BeforeClass
    public static void setUpBeforeClass() {
        nc = new NullConstraint("p");
    }

    @AfterClass
    public static void tearDownAfterClass() {
        nc = null;
    }

    @Test
    public void testConstructor() {
        try {
            new NullConstraint(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new NullConstraint("  ");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }
    }

    @Test
    public void testGetPropertyName() {
        assertEquals("p", nc.getPropertyName());
    }

    @Test
    public void testToString() {
        assertEquals("p is null", nc.toString());
    }
}
