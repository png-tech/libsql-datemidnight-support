package net.sf.jparts.libsql.mapping.types;

public final class Primitives {

    public static Object getDefaultValue(Class<?> type) {
        if (type.equals(boolean.class)) {
            return false;
        } else if (type.equals(byte.class)) {
            return (byte) 0;
        } else if (type.equals(short.class)) {
            return (short) 0;
        } else if (type.equals(int.class)) {
            return 0;
        } else if (type.equals(long.class)) {
            return 0L;
        } else if (type.equals(float.class)) {
            return 0f;
        } else if (type.equals(double.class)) {
            return 0d;
        } else if (type.equals(char.class)) {
            return '\0';
        } else {
            throw new IllegalArgumentException("Unknown primitive type: " + type);
        }
    }

    public static Object getOrDefault(Class<?> type, Object value) {
        return (value == null) ? getDefaultValue(type) : value;
    }

    private Primitives() {
    }
}
