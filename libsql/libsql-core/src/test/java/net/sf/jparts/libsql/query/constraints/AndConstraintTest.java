package net.sf.jparts.libsql.query.constraints;

import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for {@link AndConstraint}.
 */
public class AndConstraintTest {

    @Test
    public void testAnd() {
        AndConstraint ac = new AndConstraint();
        ac.setConstraints(new TxtConstraint("t1"), new TxtConstraint("t2"), new TxtConstraint("t3"));

        boolean append = ac.isAppendSpaces();
        assertTrue(append);

        String op = ac.getJoinOperator();
        assertEquals("and", op);

        List<Constraint> constraints = ac.getConstraints();
        assertNotNull(constraints);
        assertEquals(3, constraints.size());
        
        final String ok = "t1 and t2 and t3";
        String and = ac.toString();
        assertEquals(ok, and);
    }
}
