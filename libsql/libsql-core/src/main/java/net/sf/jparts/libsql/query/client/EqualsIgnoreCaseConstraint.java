package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "equalsIgnoreCase")
public class EqualsIgnoreCaseConstraint implements Constraint {

    private String property;

    private String value;

    public EqualsIgnoreCaseConstraint() {
    }

    public EqualsIgnoreCaseConstraint(String property, String value) {
        this.property = property.trim();
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String propertyName) {
        this.property = propertyName;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EqualsIgnoreCaseConstraint that = (EqualsIgnoreCaseConstraint) o;

        if (property != null ? !property.equals(that.property) : that.property != null) return false;
        //noinspection RedundantIfStatement
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = property != null ? property.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
