package net.sf.jparts.libsql.query.constraints;

public interface OnePropertyConstraint extends Constraint {

    public String getPropertyName();
}
