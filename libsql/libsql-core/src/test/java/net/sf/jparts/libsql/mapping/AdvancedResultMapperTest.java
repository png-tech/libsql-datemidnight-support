package net.sf.jparts.libsql.mapping;

import net.sf.jparts.libsql.mapping.model.test1.Ref;
import net.sf.jparts.libsql.mapping.model.test1.Root;
import net.sf.jparts.libsql.mapping.model.test2.Auto;
import net.sf.jparts.libsql.mapping.model.test3.B;
import net.sf.jparts.libsql.mapping.model.test4.D;
import net.sf.jparts.libsql.mapping.model.test4.D1;
import net.sf.jparts.libsql.mapping.model.test4.D2;
import net.sf.jparts.libsql.mapping.model.test5.E1;
import net.sf.jparts.libsql.mapping.model.test5.E2;
import net.sf.jparts.libsql.mapping.model.test5.E2N;
import net.sf.jparts.libsql.mapping.model.test6.PojoWithPrimitives;
import net.sf.jparts.libsql.mapping.model.test7.InnerPojo;
import net.sf.jparts.libsql.mapping.model.test7.OuterPojo;
import net.sf.jparts.libsql.mapping.types.TypeHandler;

import org.junit.Test;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AdvancedResultMapperTest {

    // test 1: constructors, nested objects in constructor

    private AdvancedResultMapper<Root> createMapper_test1() {
        ResultMapping refMapping = new ResultMapping.Builder("test1_ref")
                .setType(Ref.class)
                .constructorArguments(
                        new SimpleArgumentMapping.Builder("id")
                                .setType(String.class)
                                .build(),
                        new SimpleArgumentMapping.Builder("dn")
                                .setType(String.class)
                                .build()
                )
                .build();

        ResultMapping rootMapping = new ResultMapping.Builder("test1_root")
                .setType(Root.class)
                .constructorArguments(new NestedArgumentMapping.Builder(refMapping)
                        .setType(Ref.class)
                        .setColumnPrefix("pr_")
                        .build())
                .build();

        return new AdvancedResultMapper<>(rootMapping);
    }

    private void mockRsMetaData_test1(ResultSet rs) throws SQLException {
        ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

        when(meta.getColumnCount()).thenReturn(4);
        when(meta.getColumnName(1)).thenReturn("a");
        when(meta.getColumnName(2)).thenReturn("s");
        when(meta.getColumnName(3)).thenReturn("pr_id");
        when(meta.getColumnName(4)).thenReturn("pr_dn");
    }

    @Test
    public void test1_constructor_with_nested() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        mockRsMetaData_test1(rs);

        when(rs.getString("pr_id")).thenReturn("1");
        when(rs.getString("pr_dn")).thenReturn("first");
        when(rs.getString("a")).thenReturn("aaa");
        when(rs.getString("s")).thenReturn(null);

        AdvancedResultMapper<Root> mapper = createMapper_test1();
        Root root = mapper.mapRow(rs);
        System.out.println("test1_constructor_with_nested: " + root);

        assertNotNull(root);
        assertNotNull(root.getR1());
        assertEquals("1", root.getR1().getId());
        assertEquals("first", root.getR1().getDn());
        assertEquals("aaa", root.getA());
        assertNull(root.getS());
    }

    @Test
    public void test1_constructor_with_nested_empty_rs() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        mockRsMetaData_test1(rs);

        AdvancedResultMapper<Root> mapper = createMapper_test1();
        Root root = mapper.mapRow(rs);
        System.out.println("test1_constructor_with_nested_empty_rs: " + root);

        assertNotNull(root);
        assertNull(root.getR1());
        assertNull(root.getA());
        assertNull(root.getS());
    }

    // test 2: automapping

    private AdvancedResultMapper<Auto> createMapper_test2(boolean autoMapping) {
        ResultMapping mapping = new ResultMapping.Builder("test2_auto")
                .setType(Auto.class)
                .setAutoMapping(autoMapping)
                .constructorArguments(
                        new SimpleArgumentMapping.Builder("id")
                                .setType(String.class)
                                .build()
                )
                .build();

        return new AdvancedResultMapper<>(mapping);
    }

    private void mockRsMetaData_test2(ResultSet rs) throws SQLException {
        ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

        when(meta.getColumnCount()).thenReturn(1);
        when(meta.getColumnName(1)).thenReturn("id");
    }

    @Test
    public void test2_constructor_and_automapping_enabled() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        mockRsMetaData_test2(rs);

        when(rs.getString("id")).thenReturn("id_value");

        AdvancedResultMapper<Auto> mapper = createMapper_test2(true); // auto mapping enabled
        Auto a = mapper.mapRow(rs);
        System.out.println("test2_constructor_and_automapping_enabled: " + a);

        assertNotNull(a);
        assertEquals("id_value", a.getId());
    }

    @Test
    public void test2_constructor_and_automapping_disabled() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        mockRsMetaData_test2(rs);

        when(rs.getString("id")).thenReturn("id_value");

        AdvancedResultMapper<Auto> mapper = createMapper_test2(false); // auto mapping disabled
        Auto a = mapper.mapRow(rs);
        System.out.println("test2_constructor_and_automapping_disabled: " + a);

        assertNotNull(a);
        assertEquals("id_value" + Auto.SUFFIX, a.getId());
    }

    // test 3: fields from superclass

    @Test
    public void test3_fields_in_superclass() throws SQLException {
        ResultMapping mapping = new ResultMapping.Builder("test3_b")
                .setType(B.class)
                .build();

        ResultSet rs = mock(ResultSet.class);

        ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

        when(meta.getColumnCount()).thenReturn(2);
        when(meta.getColumnName(1)).thenReturn("a");
        when(meta.getColumnName(2)).thenReturn("b");

        when(rs.getString("a")).thenReturn("aaa");
        when(rs.getString("b")).thenReturn("bbb");

        AdvancedResultMapper<B> mapper = new AdvancedResultMapper<>(mapping);
        B b = mapper.mapRow(rs);
        System.out.println("test3_fields_in_superclass: " + b);

        assertNotNull(b);
        assertEquals("aaa", b.getA());
        assertEquals("bbb", b.getB());
        assertNull(b.getB_in_A()); // field 'b' exists in child class!
    }

    // test 4: discriminator and class hierarchy

    private AdvancedResultMapper<D> createMapper_test4() {
        ResultMapping d1 = new ResultMapping.Builder("test4_d1")
                .setType(D1.class)
                .setAutoMapping(false)
                .constructorArguments(
                        new SimpleArgumentMapping.Builder("value").setType(String.class).build(),
                        new SimpleArgumentMapping.Builder("d1").setType(String.class).build()
                )
                .build();

        ResultMapping d2 = new ResultMapping.Builder("test4_d2")
                .setType(D2.class)
                .setAutoMapping(false)
                .constructorArguments(
                        new SimpleArgumentMapping.Builder("value").setType(String.class).build(),
                        new SimpleArgumentMapping.Builder("d2").setType(String.class).build()
                )
                .build();

        ResultMapping d = new ResultMapping.Builder("test4_d")
                .setType(D.class)
                .setAutoMapping(false)
                .constructorArguments(new SimpleArgumentMapping.Builder("value").setType(String.class).build())
                .setDiscriminatorMapping(
                        new DiscriminatorMapping.Builder("value").addCase("d1_desc", d1).addCase("d2_desc", d2).build()
                )
                .build();

        return new AdvancedResultMapper<>(d);
    }

    private ResultSet mockRs_test4() throws SQLException {
        ResultSet rs = mock(ResultSet.class);

        ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

        when(meta.getColumnCount()).thenReturn(3);
        when(meta.getColumnName(1)).thenReturn("value");
        when(meta.getColumnName(2)).thenReturn("d1");
        when(meta.getColumnName(3)).thenReturn("d2");

        return rs;
    }

    @Test
    public void test4_d1_row() throws SQLException {
        ResultSet rs = mockRs_test4();
        when(rs.getString("value")).thenReturn("d1_desc");
        when(rs.getString("d1")).thenReturn("d1_value");

        AdvancedResultMapper<D> mapper = createMapper_test4();
        D d = mapper.mapRow(rs);
        System.out.println("test4_d1_row: " + d);

        assertNotNull(d);
        assertEquals(D1.class, d.getClass());
        assertEquals("d1_desc", d.getValue());
        assertEquals("d1_value", ((D1) d).getD1());
    }

    @Test
    public void test4_d2_row() throws SQLException {
        ResultSet rs = mockRs_test4();
        when(rs.getString("value")).thenReturn("d2_desc");
        when(rs.getString("d2")).thenReturn("d2_value");

        AdvancedResultMapper<D> mapper = createMapper_test4();
        D d = mapper.mapRow(rs);
        System.out.println("test4_d2_row: " + d);

        assertNotNull(d);
        assertEquals(D2.class, d.getClass());
        assertEquals("d2_desc", d.getValue());
        assertEquals("d2_value", ((D2) d).getD2());
    }

    @Test
    public void test4_d_row() throws SQLException {
        ResultSet rs = mockRs_test4();
        when(rs.getString("value")).thenReturn("other");

        AdvancedResultMapper<D> mapper = createMapper_test4();
        D d = mapper.mapRow(rs);
        System.out.println("test4_d_row: " + d);

        assertNotNull(d);
        assertEquals(D.class, d.getClass());
        assertEquals("other", d.getValue());
    }

    // test 5: explicit mappings

    @Test
    public void test5_explicit_mapping_simple_properties() throws SQLException {
        ResultMapping e1m = new ResultMapping.Builder("test5_e1")
                .setType(E1.class)
                .setAutoMapping(false)
                .addPropertyMapping(new SimplePropertyMapping("a", null, (TypeHandler) null))
                .addPropertyMapping(new SimplePropertyMapping("c", "b", (TypeHandler) null))
                .build();

        ResultSet rs = mock(ResultSet.class);

        ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

        when(meta.getColumnCount()).thenReturn(2);
        when(meta.getColumnName(1)).thenReturn("a");
        when(meta.getColumnName(2)).thenReturn("b");

        when(rs.getString("a")).thenReturn("aaa");
        when(rs.getString("b")).thenReturn("bbb");

        AdvancedResultMapper<E1> mapper = new AdvancedResultMapper<>(e1m);
        E1 e = mapper.mapRow(rs);
        System.out.println("test5_explicit_mapping_simple_properties: " + e);

        assertNotNull(e);
        assertEquals("aaa", e.getA());
        assertEquals("bbb", e.getC());
    }

    @Test
    public void test5_explicit_mapping_association() throws SQLException {
        ResultMapping e2nm = new ResultMapping.Builder("test5_e2n")
                .setType(E2N.class)
                .build();

        ResultMapping e2m = new ResultMapping.Builder("test5_e2")
                .setType(E2.class)
                .setAutoMapping(false)
                .addPropertyMapping(new AssociationMapping.Builder("n", e2nm).build())
                .build();

        ResultSet rs = mock(ResultSet.class);

        ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

        when(meta.getColumnCount()).thenReturn(2);
        when(meta.getColumnName(1)).thenReturn("a");
        when(meta.getColumnName(2)).thenReturn("b");

        when(rs.getString("a")).thenReturn("aaa");
        when(rs.getString("b")).thenReturn("bbb");

        AdvancedResultMapper<E2> mapper = new AdvancedResultMapper<>(e2m);
        E2 e = mapper.mapRow(rs);
        System.out.println("test5_explicit_mapping_association: " + e);

        assertNotNull(e);
        assertNotNull(e.getN());
        assertEquals("aaa", e.getN().getA());
        assertEquals("bbb", e.getN().getB());
    }

    // test 6: primitives

    @Test
    public void test6_primitives() throws Exception {
        ResultMapping m = new ResultMapping.Builder("test6")
                .setType(PojoWithPrimitives.class)
                .build();

        ResultSet rs = mock(ResultSet.class);

        ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

        when(meta.getColumnCount()).thenReturn(4);
        when(meta.getColumnName(1)).thenReturn("f");
        when(meta.getColumnName(2)).thenReturn("g");
        when(meta.getColumnName(3)).thenReturn("i");
        when(meta.getColumnName(4)).thenReturn("j");

        when(rs.getBoolean("f")).thenReturn(true);
        when(rs.getObject("g")).thenReturn(null);
        when(rs.getObject("i")).thenReturn(null);
        when(rs.getInt("j")).thenReturn(17);

        AdvancedResultMapper<PojoWithPrimitives> mapper = new AdvancedResultMapper<>(m);
        PojoWithPrimitives p = mapper.mapRow(rs);

        System.out.println(p);
        assertTrue(p.isF());
        assertFalse(p.isG());
        assertEquals(0, p.getI());
        assertEquals(17, p.getJ());
        assertEquals(-100, p.getX());
    }

    // test 7: primitives and nulls

    @Test
    public void test7_primitive_columns_all_null_for_nested() throws Exception {
        ResultMapping innerMapping = new ResultMapping.Builder("inner")
                .setType(InnerPojo.class)
                .setAutoMapping(true)
                .build();
        ResultMapping outMapping = new ResultMapping.Builder("test7")
                .setType(OuterPojo.class)
                .setAutoMapping(true)
                .addPropertyMapping(new AssociationMapping.Builder("inner", innerMapping)
                        .setColumnPrefix("p_")
                        .build())
                .build();

        ResultSet rs = mock(ResultSet.class);

        ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

        when(meta.getColumnCount()).thenReturn(3);
        when(meta.getColumnName(1)).thenReturn("name");
        when(meta.getColumnName(2)).thenReturn("p_i1");
        when(meta.getColumnName(3)).thenReturn("p_i2");

        when(rs.getString("name")).thenReturn("testname");
        when(rs.wasNull()).thenReturn(true);

        AdvancedResultMapper<OuterPojo> mapper = new AdvancedResultMapper<>(outMapping);
        OuterPojo o = mapper.mapRow(rs);

        System.out.println("test7_primitive_columns_all_null_for_nested: " + o);
        assertNotNull(o);
        assertNull(o.getInner());
        assertEquals("testname", o.getName());
    }

    @Test
    public void test7_null_values_for_primitive_columns_nested() throws Exception {
        ResultMapping innerMapping = new ResultMapping.Builder("inner")
                .setType(InnerPojo.class)
                .setAutoMapping(true)
                .build();
        ResultMapping outMapping = new ResultMapping.Builder("test7")
                .setType(OuterPojo.class)
                .setAutoMapping(true)
                .addPropertyMapping(new AssociationMapping.Builder("inner", innerMapping)
                        .setColumnPrefix("p_")
                        .build())
                .build();

        ResultSet rs = mock(ResultSet.class);

        ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

        when(meta.getColumnCount()).thenReturn(9);
        when(meta.getColumnName(1)).thenReturn("name");
        when(meta.getColumnName(2)).thenReturn("p_i1");
        when(meta.getColumnName(3)).thenReturn("p_i2");
        when(meta.getColumnName(4)).thenReturn("p_b");
        when(meta.getColumnName(5)).thenReturn("p_bt");
        when(meta.getColumnName(6)).thenReturn("p_s");
        when(meta.getColumnName(7)).thenReturn("p_l");
        when(meta.getColumnName(8)).thenReturn("p_f");
        when(meta.getColumnName(9)).thenReturn("p_d");

        when(rs.getString("name")).thenReturn("testname");
        when(rs.getInt("p_i2")).thenReturn(17);
        when(rs.wasNull()).thenReturn(true, false, true);

        AdvancedResultMapper<OuterPojo> mapper = new AdvancedResultMapper<>(outMapping);
        OuterPojo o = mapper.mapRow(rs);

        System.out.println("test7_null_values_for_primitive_columns_nested: " + o);
        assertNotNull(o);
        assertEquals("testname", o.getName());
        assertNotNull(o.getInner());
        assertEquals(0, o.getInner().getI1());
        assertEquals(17, o.getInner().getI2());
        assertEquals(false, o.getInner().isB());
        assertEquals((byte) 0, o.getInner().getBt());
        assertEquals((short) 0, o.getInner().getS());
        assertEquals(0L, o.getInner().getL());
        assertEquals(0.0f, o.getInner().getF(), 0.0001f);
        assertEquals(0.0d, o.getInner().getD(), 0.0001d);
    }

    @Test
    public void test7_read_null_value_from_primitive() throws Exception {
        ResultMapping mixedMapping = new ResultMapping.Builder("inner")
                        .setType(InnerPojo.class)
                        .setAutoMapping(true)
                        .build();

                ResultSet rs = mock(ResultSet.class);

                ResultSetMetaData meta = mock(ResultSetMetaData.class);
        when(rs.getMetaData()).thenReturn(meta);

                when(meta.getColumnCount()).thenReturn(3);
        when(meta.getColumnName(1)).thenReturn("b");
        when(meta.getColumnName(2)).thenReturn("i1");
        when(meta.getColumnName(3)).thenReturn("i2");

                when(rs.getBoolean("b")).thenReturn(true);
        when(rs.getInt("i1")).thenReturn(0);
        when(rs.getInt("i2")).thenReturn(5);
        when(rs.wasNull()).thenReturn(false, true, false);

                AdvancedResultMapper<InnerPojo> mapper = new AdvancedResultMapper<>(mixedMapping);
        InnerPojo o = mapper.mapRow(rs);
        System.out.println("test7_read_null_value_from_primitive: " + o);

                assertEquals(true, o.isB());
        assertEquals(0, o.getI1());
        assertEquals(5, o.getI2());

        verify(rs, times(3)).wasNull();
    }
}
