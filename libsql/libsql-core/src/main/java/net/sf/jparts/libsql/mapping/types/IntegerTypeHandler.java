package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IntegerTypeHandler implements TypeHandler<Integer> {

    @Override
    public Integer getResult(ResultSet rs, String column) throws SQLException {
        int i = rs.getInt(column);
        return (rs.wasNull()) ? null : i;
    }
}
