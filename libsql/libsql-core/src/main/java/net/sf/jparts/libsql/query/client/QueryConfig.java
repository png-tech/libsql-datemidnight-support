package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@XmlRootElement(name = "queryConfig")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryConfig implements Serializable {

    private static final long serialVersionUID = -5062238199963403734L;

    private int firstResult = 0;

    private int maxResults = Integer.MAX_VALUE;

    @XmlElementWrapper(name = "parameters")
    @XmlElement(name = "parameter")
    private List<Parameter> parameters = new ArrayList<>();

    @XmlElementWrapper(name = "orders")
    @XmlElement(name = "order")
    private List<Order> orders = new ArrayList<>();

    @XmlElementWrapper(name = "constraints")
    @XmlElementRefs({
            @XmlElementRef(type = AndConstraint.class)
            ,@XmlElementRef(type = ConjunctionConstraint.class)
            ,@XmlElementRef(type = OrConstraint.class)
            ,@XmlElementRef(type = DisjunctionConstraint.class)
            ,@XmlElementRef(type = SimpleConstraint.class)
            ,@XmlElementRef(type = ContainsConstraint.class)
            ,@XmlElementRef(type = NullConstraint.class)
            ,@XmlElementRef(type = NotNullConstraint.class)
            ,@XmlElementRef(type = NotConstraint.class)
            ,@XmlElementRef(type = InConstraint.class)
            ,@XmlElementRef(type = InIgnoreCaseConstraint.class)
            ,@XmlElementRef(type = BetweenConstraint.class)
            ,@XmlElementRef(type = StartsWithConstraint.class)
            ,@XmlElementRef(type = StartsWithCaseSensitiveConstraint.class)
            ,@XmlElementRef(type = EndsWithConstraint.class)
            ,@XmlElementRef(type = EndsWithCaseSensitiveConstraint.class)
            ,@XmlElementRef(type = EqualsIgnoreCaseConstraint.class)
    })
    private List<Constraint> constraints = new ArrayList<>();

    public int getFirstResult() {
        return firstResult;
    }

    public QueryConfig setFirstResult(int firstResult) {
        if (firstResult < 0) {
            throw new IllegalArgumentException("firstResult must be >= 0");
        }
        this.firstResult = firstResult;
        return this;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public QueryConfig setMaxResults(int maxResults) {
        if (maxResults < 1) {
            throw new IllegalArgumentException("maxResults must be > 0");
        }
        this.maxResults = maxResults;
        return this;
    }

    public QueryConfig setParameter(String name, String value) {
        parameters.add(new Parameter(name, value));
        return this;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public String getParameter(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("name must not be empty or null");
        }
        for (Parameter p : parameters) {
            if (p.getName().equals(name)) {
                return p.getValue();
            }
        }
        return null;
    }

    public List<String> getParameterValues(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("name must not be empty or null");
        }
        List<String> values = new ArrayList<>();
        for (Parameter p : parameters) {
            if (p.getName().equals(name)) {
                values.add(p.getValue());
            }
        }
        return (values.isEmpty()) ? null : values;
    }

    public QueryConfig orderBy(List<Order> orders) {
        return orderBy0(orders);
    }

    public QueryConfig orderBy(Order... orders) {
        return orderBy0(Arrays.asList(orders));
    }

    public List<Order> getOrders() {
        return this.orders;
    }

    public QueryConfig where(List<Constraint> constraints) {
        return where0(constraints);
    }

    public QueryConfig where(Constraint... constraints) {
        return where0(Arrays.asList(constraints));
    }

    public List<Constraint> getConstraints() {
        return constraints;
    }

    protected QueryConfig orderBy0(Iterable<Order> i) {
        this.orders.clear();
        if (i != null) {
            for (Order o : i) {
                if (o != null) {
                    this.orders.add(o);
                }
            }
        }
        return this;
    }

    protected QueryConfig where0(Iterable<Constraint> i) {
        this.constraints.clear();
        if (i != null) {
            for (Constraint c : i) {
                if (c != null) {
                    this.constraints.add(c);
                }
            }
        }
        return this;
    }

}