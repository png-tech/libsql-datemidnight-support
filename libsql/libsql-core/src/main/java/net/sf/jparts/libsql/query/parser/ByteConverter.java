package net.sf.jparts.libsql.query.parser;

public class ByteConverter extends AbstractIntegerConverter<Byte> {

    public ByteConverter() {
    }

    public ByteConverter(boolean safe, Byte errorValue, Byte nullValue, int radix) {
        super(safe, errorValue, nullValue, radix);
    }

    @Override
    protected Byte tryCreate(String value, int radix) throws NumberFormatException {
        return Byte.valueOf(value, radix);
    }
}
