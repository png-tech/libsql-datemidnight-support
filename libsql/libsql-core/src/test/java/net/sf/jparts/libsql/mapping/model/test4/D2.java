package net.sf.jparts.libsql.mapping.model.test4;

public class D2 extends D {

    private String d2;

    public D2(String value, String d2) {
        super(value);
        this.d2 = d2;
    }

    public String getD2() {
        return d2;
    }

    @Override
    public String toString() {
        return "D2{" +
                "d2='" + d2 + '\'' +
                "} " + super.toString();
    }
}
