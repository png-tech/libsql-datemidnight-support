package net.sf.jparts.libsql.mapping.model.test1;

public class Root {

    private String a;

    private String s;

    private Ref r1;

    public Root(Ref r1) {
        this.r1 = r1;
    }

    public String getA() {
        return a;
    }

    public String getS() {
        return s;
    }

    public Ref getR1() {
        return r1;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Root");
        sb.append("{a='").append(a).append('\'');
        sb.append(", s='").append(s).append('\'');
        sb.append(", r1=").append(r1);
        sb.append('}');
        return sb.toString();
    }
}
