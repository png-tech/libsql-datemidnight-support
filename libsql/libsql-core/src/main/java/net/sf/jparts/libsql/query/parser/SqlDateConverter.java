package net.sf.jparts.libsql.query.parser;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class SqlDateConverter extends AbstractDateConverter<Date> {

    public static final String DEFAULT_FORMAT = "yyyy-MM-dd";

    public SqlDateConverter() {
        super(DEFAULT_FORMAT);
    }

    public SqlDateConverter(final String pattern) {
        super(pattern);
    }

    public SqlDateConverter(boolean safe, final String pattern, Date errorValue, Date nullValue) {
        super(safe, pattern, errorValue, nullValue);
    }

    @Override
    protected Date tryParse(String value, SimpleDateFormat format) throws ParseException {
        return new Date(format.parse(value).getTime());
    }
}
