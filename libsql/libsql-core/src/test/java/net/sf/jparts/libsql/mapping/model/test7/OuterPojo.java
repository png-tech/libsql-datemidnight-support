package net.sf.jparts.libsql.mapping.model.test7;

public class OuterPojo {
    private String name;
    private InnerPojo inner;

    public InnerPojo getInner() {
        return inner;
    }

    public void setInner(InnerPojo inner) {
        this.inner = inner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "OuterPojo{" +
                "inner=" + inner +
                ", name='" + name + '\'' +
                '}';
    }
}
