package net.sf.jparts.libsql.mapping.types;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BigIntegerTypeHandler implements TypeHandler<BigInteger> {

    @Override
    public BigInteger getResult(ResultSet rs, String column) throws SQLException {
        // return rs.getObject(column, BigInteger.class); // fails in oracle jdbc driver :/
        BigDecimal v = rs.getBigDecimal(column);
        return (v == null) ? null : v.toBigInteger();
    }
}
