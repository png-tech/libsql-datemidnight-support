package net.sf.jparts.libsql.spring.template;

public class MaxQuerySizeCacheFilter implements CacheFilter {

    private final long maxSize;

    public MaxQuerySizeCacheFilter(long maxSize) {
        if (maxSize <= 1)
            throw new IllegalArgumentException("maxSize must be greater than 1");
        this.maxSize = maxSize;
    }

    @Override
    public boolean satisfied(String sql) throws CacheFilterException {
        if (sql == null)
            throw new CacheFilterException("sql is null");
        return sql.length() <= maxSize;
    }
}
