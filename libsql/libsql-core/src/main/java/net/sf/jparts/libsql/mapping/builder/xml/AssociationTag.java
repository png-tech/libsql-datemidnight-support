package net.sf.jparts.libsql.mapping.builder.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssociationMapping")
public class AssociationTag {

    @XmlAttribute(name = "name", required = true)
    public String name;

    @XmlAttribute(name = "result", required = true)
    public String result;

    @XmlAttribute(name = "columnPrefix")
    public String columnPrefix;

}
