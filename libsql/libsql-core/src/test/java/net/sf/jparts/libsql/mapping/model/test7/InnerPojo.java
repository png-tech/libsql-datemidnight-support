package net.sf.jparts.libsql.mapping.model.test7;

public class InnerPojo {

    private int i1 = -5;
    private int i2 = -6;

    private boolean b = true;
    private byte bt = -12;
    private short s = -22;
    private long l = -1111;
    private float f = -110.1f;
    private double d = -21.2d;

    public boolean isB() {
        return b;
    }

    public void setB(boolean b) {
        this.b = b;
    }

    public byte getBt() {
        return bt;
    }

    public void setBt(byte bt) {
        this.bt = bt;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public int getI1() {
        return i1;
    }

    public void setI1(int i1) {
        this.i1 = i1;
    }

    public int getI2() {
        return i2;
    }

    public void setI2(int i2) {
        this.i2 = i2;
    }

    public float getF() {
        return f;
    }

    public void setF(float f) {
        this.f = f;
    }

    public long getL() {
        return l;
    }

    public void setL(long l) {
        this.l = l;
    }

    public short getS() {
        return s;
    }

    public void setS(short s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return "InnerPojo{" +
                "b=" + b +
                ", i1=" + i1 +
                ", i2=" + i2 +
                ", bt=" + bt +
                ", s=" + s +
                ", l=" + l +
                ", f=" + f +
                ", d=" + d +
                '}';
    }
}
