package net.sf.jparts.libsql.mapping.model.test5;

public class E2N {

    private String a;

    private String b;

    public String getA() {
        return a;
    }

    public String getB() {
        return b;
    }

    @Override
    public String toString() {
        return "E2N{" +
                "a='" + a + '\'' +
                ", b='" + b + '\'' +
                '}';
    }
}
