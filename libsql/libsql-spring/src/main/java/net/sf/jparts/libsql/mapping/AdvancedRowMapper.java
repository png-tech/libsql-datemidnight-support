package net.sf.jparts.libsql.mapping;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Adapter for use {@link AdvancedResultMapper} with Spring JDBC.
 * @param <T>
 */
public class AdvancedRowMapper<T> extends AdvancedResultMapper<T> implements RowMapper<T> {

    public AdvancedRowMapper(String resultId) {
        super(resultId);
    }

    public AdvancedRowMapper(ResultMapping mapping) {
        super(mapping);
    }

    public AdvancedRowMapper(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public T mapRow(ResultSet rs, int rowNum) throws SQLException {
        return mapRow(rs);
    }
}
