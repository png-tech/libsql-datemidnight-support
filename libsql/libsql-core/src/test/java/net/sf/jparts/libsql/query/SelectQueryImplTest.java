package net.sf.jparts.libsql.query;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import net.sf.jparts.libsql.query.constraints.Constraint;
import net.sf.jparts.libsql.query.constraints.Order;
import net.sf.jparts.libsql.query.constraints.TxtConstraint;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for {@link SelectQueryImpl}.
 */
public class SelectQueryImplTest {

    @Test
    public void testConstructor() {
        try {
            new SelectQueryImpl<>(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new SelectQueryImpl<>("  ");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            SelectQueryImpl<?> q = new SelectQueryImpl<>(" aaa");
            assertEquals("aaa", q.getName());
        } catch (Exception ex) {
            fail("Unexpected exception");
        }
    }

    @Test
    public void testSetFirstResult() {
        SelectQuery<?> q = new SelectQueryImpl<>("a");
        try {
            q.setFirstResult(-1);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        q.setFirstResult(12);
        assertEquals(12, q.getFirstResult());
    }

    @Test
    public void testSetMaxResults() {
        SelectQuery<?> q = new SelectQueryImpl<>("a");
        try {
            q.setMaxResults(0);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        q.setMaxResults(12);
        assertEquals(12, q.getMaxResults());
    }

    @Test
    public void testSetHint() {
        SelectQuery<?> q = new SelectQueryImpl<>("a");
        try {
            q.setHint(null, 123);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            q.setHint(" ", 123);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        q.setHint("h", 123);
        assertEquals(123, q.getHintValue("h"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSetResultClass() {
        SelectQueryImpl<Void> q = new SelectQueryImpl<>("a");
        try {
            q.setResultClass(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        q.setResultClass(Void.class);
        assertEquals(Void.class, q.getResultClass());
    }

    @Test
    public void testSetQueryString() {
        SelectQueryImpl<?> q = new SelectQueryImpl<>("a");
        try {
            q.setQueryString(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            q.setQueryString(" ");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        q.setQueryString(" h ");
        assertEquals("h", q.getQueryString());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testOrderByList() {
        SelectQueryImpl<?> q = new SelectQueryImpl<>("a");

        List<Order> o1 = Arrays.asList(Order.asc("id"), Order.desc("name"));
        q.orderBy(o1);

        List<Order> o2 = q.getOrderBy();
        assertNotNull(o2);
        assertArrayEquals(o1.toArray(), o2.toArray());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testWhereList() {
        SelectQueryImpl<?> q = new SelectQueryImpl<>("a");

        List<Constraint> c1 = Arrays.<Constraint>asList(new TxtConstraint("t1"), new TxtConstraint("t2"));
        q.where(c1);

        List<Constraint> c2 = q.getConstraints();
        assertNotNull(c2);
        assertArrayEquals(c1.toArray(), c2.toArray());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void setSetParameterByIndex() {
        SelectQueryImpl<?> q = new SelectQueryImpl<>("a");
        q.setParameter(1, null);

        Set<QueryParameter<?>> params = q.getParameters();
        assertEquals(1, params.size());
        assertEquals(Object.class, params.iterator().next().getParameterType());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void setSetParameterByName() {
        SelectQueryImpl<?> q = new SelectQueryImpl<>("a");
        q.setParameter("qwerty", null);

        Set<QueryParameter<?>> params = q.getParameters();
        assertEquals(1, params.size());
        assertEquals(Object.class, params.iterator().next().getParameterType());
        assertEquals("qwerty", params.iterator().next().getName());
    }

}
