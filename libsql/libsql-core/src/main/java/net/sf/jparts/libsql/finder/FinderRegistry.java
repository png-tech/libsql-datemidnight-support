package net.sf.jparts.libsql.finder;

import net.sf.jparts.libsql.common.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Contains all configured {@link Finder} instances. Finders can be added and removed at runtime.
 * Reads <code>META-INF/libsql/FinderRegistry.properties</code> configuration file at startup.
 */
public class FinderRegistry {

    /**
     * Logger.
     */
    private static final Logger logger = LoggerFactory.getLogger(FinderRegistry.class);

    private static final FinderRegistry instance = new FinderRegistry();

    /**
     * Contains all configured finders.
     */
    private Map<String, Finder> findersMap = new ConcurrentHashMap<>();

    /**
     * Returns current FinderRegistry instance.
     * @return not <code>null</code>.
     */
    public static FinderRegistry getInstance() {
        return instance;
    }

    /**
     * Adds finder to registry.
     *
     * @param name finder name, not empty string and not <code>null</code>.
     * @param finder finder instance.
     *
     * @throws IllegalArgumentException if <code>name</code> is empty or <code>null</code>
     *  or <code>finder</code> is <code>null</code>. 
     */
    public void add(String name, Finder finder) {
        checkName(name);
        if (finder == null) {
            throw new IllegalArgumentException("finder must not be null");
        }
        findersMap.put(name.trim(), finder);
    }

    /**
     * Returns list of all configured finder names.
     *
     * @return not <code>null</code>.
     */
    public List<String> getNames() {
        return new ArrayList<>(findersMap.keySet());
    }

    /**
     * Returns {@link Finder} instance by name.
     *
     * @param name finder name, must not be empty string or <code>null</code>. Whitespace will be trimmed.
     * @return not <code>null</code>.
     *
     * @throws FinderNotFoundException if finder with given name does not exists in registry.
     */
    public Finder get(String name) {
        checkName(name);
        Finder f = findersMap.get(name.trim());
        if (f == null) {
            throw new FinderNotFoundException("finder with name " + name + " was not found");
        }
        return f;
    }

    /**
     * Removes finder with given name from registry.
     * @param name finder name.
     *
     * @throws IllegalArgumentException if <code>name</code> is empty string or <code>null</code>.
     */
    public void remove(String name) {
        checkName(name);
        findersMap.remove(name.trim());
    }

    private void checkName(String name) {
        if (name == null || name.trim().isEmpty()) {
            throw new IllegalArgumentException("name must not be empty or null");
        }
    }

    private void loadFinders(Properties p) {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        StringBuilder sb = new StringBuilder();

        // iterate over properties
        for (String finderName : p.stringPropertyNames()) {
            // check property key and value
            if (finderName == null || finderName.trim().isEmpty()) {
                continue;
            }
            String finderClassName = p.getProperty(finderName);
            if (finderClassName == null || finderClassName.trim().isEmpty()) {
                logger.warn("Class is not defined for finder with name {}", finderName);
                continue;
            }
            finderClassName = finderClassName.trim();

            // load finder class, create finder instance and
            // register finder
            try {
                Class<?> finderClass = cl.loadClass(finderClassName);
                Finder f = (Finder) finderClass.newInstance();
                add(finderName, f);
            } catch (Exception ex) {
                logger.error("Finder class {} can not be loaded or initiated", finderClassName);
                continue;
            }
            sb.append(finderName).append(" : ").append(finderClassName).append("\n");
        }

        logger.info("List of loaded finders:\n{} ", sb.toString());
    }

    /**
     * <code>FinderRegistry</code> is singleton because there is no need
     * to use other implementations.
     *
     * @throws RuntimeException if configuration is not found.
     */
    private FinderRegistry() {
        // read configuration
        Properties p = Configuration.get(FinderRegistry.class);

        // initialize default finder
        if (p == null) {
            final String msg = "Configuration of FinderRegistry is not found";
            logger.error(msg);
            throw new RuntimeException(msg);
        }

        loadFinders(p);
    }

}
