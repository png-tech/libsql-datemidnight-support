package net.sf.jparts.libsql.spring.template;

import net.sf.jparts.libsql.loader.QueryLoadException;
import net.sf.jparts.libsql.loader.QueryLoaderService;

/**
 * Uses {@link QueryLoaderService} to load query from external source by its name.
 */
public class LibsqlQuerySupplier implements QueryModifier {

    @Override
    public String apply(String sql) throws QueryModificationException {
        try {
            return QueryLoaderService.getLoader().load(sql);
        } catch (QueryLoadException ex) {
            throw new QueryModificationException("Can't load query using QueryLoaderService: " + ex.getMessage(), ex);
        }
    }
}
