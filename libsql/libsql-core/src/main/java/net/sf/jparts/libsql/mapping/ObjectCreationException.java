package net.sf.jparts.libsql.mapping;

/**
 * Thrown to indicate that {@link ObjectFactory} can't create new instance.
 */
public final class ObjectCreationException extends RuntimeException {

    public ObjectCreationException(String message, Throwable cause) {
        super(message, cause);
    }
}
