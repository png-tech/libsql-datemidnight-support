package net.sf.jparts.libsql.query.constraints;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for {@link Constraints}.
 */
public class ConstraintsTest {

    private static TxtConstraint tc = new TxtConstraint("tc");

    @Test
    public void testNot() {
        Constraint c = Constraints.not(tc);
        assertNotNull(c);
        assertTrue(c instanceof NotConstraint);
        NotConstraint nc = (NotConstraint) c;
        assertEquals(tc, nc.getConstraint());
    }

    @Test
    public void testEq() {
        Constraint c = Constraints.eq("p", "v");
        assertNotNull(c);
        assertTrue(c instanceof SimpleConstraint);
        SimpleConstraint sc = (SimpleConstraint) c;
        assertEquals("p", sc.getPropertyName());
        assertEquals("=", sc.getOperator());
        assertEquals("v", sc.getValue());
    }

    @Test
    public void testNe() {
        Constraint c = Constraints.ne("p", "v");
        assertNotNull(c);
        assertTrue(c instanceof SimpleConstraint);
        SimpleConstraint sc = (SimpleConstraint) c;
        assertEquals("p", sc.getPropertyName());
        assertEquals("<>", sc.getOperator());
        assertEquals("v", sc.getValue());
    }

    @Test
    public void testLike() {
        Constraint c = Constraints.like("p", "v");
        assertNotNull(c);
        assertTrue(c instanceof SimpleConstraint);
        SimpleConstraint sc = (SimpleConstraint) c;
        assertEquals("p", sc.getPropertyName());
        assertEquals("like", sc.getOperator());
        assertEquals("v", sc.getValue());
    }

    @Test
    public void testGt() {
        Constraint c = Constraints.gt("p", "v");
        assertNotNull(c);
        assertTrue(c instanceof SimpleConstraint);
        SimpleConstraint sc = (SimpleConstraint) c;
        assertEquals("p", sc.getPropertyName());
        assertEquals(">", sc.getOperator());
        assertEquals("v", sc.getValue());
    }

    @Test
    public void testLt() {
        Constraint c = Constraints.lt("p", "v");
        assertNotNull(c);
        assertTrue(c instanceof SimpleConstraint);
        SimpleConstraint sc = (SimpleConstraint) c;
        assertEquals("p", sc.getPropertyName());
        assertEquals("<", sc.getOperator());
        assertEquals("v", sc.getValue());
    }

    @Test
    public void testGe() {
        Constraint c = Constraints.ge("p", "v");
        assertNotNull(c);
        assertTrue(c instanceof SimpleConstraint);
        SimpleConstraint sc = (SimpleConstraint) c;
        assertEquals("p", sc.getPropertyName());
        assertEquals(">=", sc.getOperator());
        assertEquals("v", sc.getValue());
    }

    @Test
    public void testLe() {
        Constraint c = Constraints.le("p", "v");
        assertNotNull(c);
        assertTrue(c instanceof SimpleConstraint);
        SimpleConstraint sc = (SimpleConstraint) c;
        assertEquals("p", sc.getPropertyName());
        assertEquals("<=", sc.getOperator());
        assertEquals("v", sc.getValue());
    }

    @Test
    public void testEqProperty() {
        Constraint c = Constraints.eqProperty("p1", "p2");
        assertNotNull(c);
        assertTrue(c instanceof PropertyConstraint);
        PropertyConstraint sc = (PropertyConstraint) c;
        assertEquals("p1", sc.getPropertyName());
        assertEquals("=", sc.getOperator());
        assertEquals("p2", sc.getOtherPropertyName());
    }

    @Test
    public void testNeProperty() {
        Constraint c = Constraints.neProperty("p1", "p2");
        assertNotNull(c);
        assertTrue(c instanceof PropertyConstraint);
        PropertyConstraint sc = (PropertyConstraint) c;
        assertEquals("p1", sc.getPropertyName());
        assertEquals("<>", sc.getOperator());
        assertEquals("p2", sc.getOtherPropertyName());
    }

    @Test
    public void testGtProperty() {
        Constraint c = Constraints.gtProperty("p1", "p2");
        assertNotNull(c);
        assertTrue(c instanceof PropertyConstraint);
        PropertyConstraint sc = (PropertyConstraint) c;
        assertEquals("p1", sc.getPropertyName());
        assertEquals(">", sc.getOperator());
        assertEquals("p2", sc.getOtherPropertyName());
    }

    @Test
    public void testLtProperty() {
        Constraint c = Constraints.ltProperty("p1", "p2");
        assertNotNull(c);
        assertTrue(c instanceof PropertyConstraint);
        PropertyConstraint sc = (PropertyConstraint) c;
        assertEquals("p1", sc.getPropertyName());
        assertEquals("<", sc.getOperator());
        assertEquals("p2", sc.getOtherPropertyName());
    }

    @Test
    public void testGeProperty() {
        Constraint c = Constraints.geProperty("p1", "p2");
        assertNotNull(c);
        assertTrue(c instanceof PropertyConstraint);
        PropertyConstraint sc = (PropertyConstraint) c;
        assertEquals("p1", sc.getPropertyName());
        assertEquals(">=", sc.getOperator());
        assertEquals("p2", sc.getOtherPropertyName());
    }

    @Test
    public void testLeProperty() {
        Constraint c = Constraints.leProperty("p1", "p2");
        assertNotNull(c);
        assertTrue(c instanceof PropertyConstraint);
        PropertyConstraint sc = (PropertyConstraint) c;
        assertEquals("p1", sc.getPropertyName());
        assertEquals("<=", sc.getOperator());
        assertEquals("p2", sc.getOtherPropertyName());
    }

    @Test
    public void testIsNull() {
        Constraint c = Constraints.isNull("p");
        assertNotNull(c);
        assertTrue(c instanceof NullConstraint);
        NullConstraint nc = (NullConstraint) c;
        assertEquals("p", nc.getPropertyName());
    }

    @Test
    public void testIsNotNull() {
        Constraint c = Constraints.isNotNull("p");
        assertNotNull(c);
        assertTrue(c instanceof NotNullConstraint);
        NotNullConstraint nc = (NotNullConstraint) c;
        assertEquals("p", nc.getPropertyName());
    }

    @Test
    public void testBetween() {
        Constraint c = Constraints.between("p", "low", "high");
        assertNotNull(c);
        assertTrue(c instanceof BetweenConstraint);
        BetweenConstraint bc = (BetweenConstraint) c;
        assertEquals("p", bc.getPropertyName());
        assertEquals("low", bc.getLow());
        assertEquals("high", bc.getHigh());
    }

    @Test
    public void testConjunction() {
        Constraint[] c1 = {tc, tc};
        Constraint c = Constraints.conjunction(c1);
        assertNotNull(c);
        assertTrue(c instanceof ConjunctionConstraint);
        ConjunctionConstraint ac = (ConjunctionConstraint) c;

        List<Constraint> c2 = ac.getConstraints();
        assertArrayEquals(c1, c2.toArray());
    }

    @Test
    public void testConjunctionList() {
        List<Constraint> c1 = Arrays.<Constraint>asList(tc, tc);
        Constraint c = Constraints.conjunction(c1);
        assertNotNull(c);
        assertTrue(c instanceof ConjunctionConstraint);
        ConjunctionConstraint ac = (ConjunctionConstraint) c;

        List<Constraint> c2 = ac.getConstraints();
        assertArrayEquals(c1.toArray(), c2.toArray());
    }

    @Test
    public void testAnd() {
        Constraint[] c1 = {tc, tc};
        Constraint c = Constraints.and(c1);
        assertNotNull(c);
        assertTrue(c instanceof AndConstraint);
        AndConstraint ac = (AndConstraint) c;

        List<Constraint> c2 = ac.getConstraints();
        assertArrayEquals(c1, c2.toArray());
    }

    @Test
    public void testAndList() {
        List<Constraint> c1 = Arrays.<Constraint>asList(tc, tc);
        Constraint c = Constraints.and(c1);
        assertNotNull(c);
        assertTrue(c instanceof AndConstraint);
        AndConstraint ac = (AndConstraint) c;

        List<Constraint> c2 = ac.getConstraints();
        assertArrayEquals(c1.toArray(), c2.toArray());
    }

    @Test
    public void testDisjunction() {
        Constraint[] c1 = {tc, tc};
        Constraint c = Constraints.disjunction(c1);
        assertNotNull(c);
        assertTrue(c instanceof DisjunctionConstraint);
        DisjunctionConstraint ac = (DisjunctionConstraint) c;

        List<Constraint> c2 = ac.getConstraints();
        assertArrayEquals(c1, c2.toArray());
    }

    @Test
    public void testDisjunctionList() {
        List<Constraint> c1 = Arrays.<Constraint>asList(tc, tc);
        Constraint c = Constraints.disjunction(c1);
        assertNotNull(c);
        assertTrue(c instanceof DisjunctionConstraint);
        DisjunctionConstraint ac = (DisjunctionConstraint) c;

        List<Constraint> c2 = ac.getConstraints();
        assertArrayEquals(c1.toArray(), c2.toArray());
    }

    @Test
    public void testOr() {
        Constraint[] c1 = {tc, tc};
        Constraint c = Constraints.or(c1);
        assertNotNull(c);
        assertTrue(c instanceof OrConstraint);
        OrConstraint ac = (OrConstraint) c;

        List<Constraint> c2 = ac.getConstraints();
        assertArrayEquals(c1, c2.toArray());
    }

    @Test
    public void testOrList() {
        List<Constraint> c1 = Arrays.<Constraint>asList(tc, tc);
        Constraint c = Constraints.or(c1);
        assertNotNull(c);
        assertTrue(c instanceof OrConstraint);
        OrConstraint ac = (OrConstraint) c;

        List<Constraint> c2 = ac.getConstraints();
        assertArrayEquals(c1.toArray(), c2.toArray());
    }

    @Test
    public void testSql() {
        Constraint c = Constraints.sql("qweqwe");
        assertNotNull(c);
        assertTrue(c instanceof SqlConstraint);
        SqlConstraint sq = (SqlConstraint) c;
        assertEquals("qweqwe", sq.toString());
    }

    @Test
    public void testSimpleConstraint() {
        Constraint c = Constraints.simpleConstraint("p", "customOp", "v");
        assertNotNull(c);
        assertTrue(c instanceof SimpleConstraint);
        SimpleConstraint sc = (SimpleConstraint) c;
        assertEquals("p", sc.getPropertyName());
        assertEquals("customOp", sc.getOperator());
        assertEquals("v", sc.getValue());
    }

    @Test
    public void testPropertyConstraint() {
        Constraint c = Constraints.propertyConstraint("p1", "lala", "p2");
        assertNotNull(c);
        assertTrue(c instanceof PropertyConstraint);
        PropertyConstraint sc = (PropertyConstraint) c;
        assertEquals("p1", sc.getPropertyName());
        assertEquals("lala", sc.getOperator());
        assertEquals("p2", sc.getOtherPropertyName());
    }

    @Test
    public void testIn() {
        Object[] v1 = {1, 2};
        Constraint c = Constraints.in("p", v1);
        assertNotNull(c);
        assertTrue(c instanceof InConstraint);
        InConstraint ic = (InConstraint) c;
        assertEquals("p", ic.getPropertyName());
        assertArrayEquals(v1, ic.getValues().toArray());
    }

    @Test
    public void testInList() {
        List<Object> v1 = Arrays.<Object>asList(1, 2);
        Constraint c = Constraints.in("p", v1);
        assertNotNull(c);
        assertTrue(c instanceof InConstraint);
        InConstraint ic = (InConstraint) c;
        assertEquals("p", ic.getPropertyName());
        assertArrayEquals(v1.toArray(), ic.getValues().toArray());
    }

    @Test
    public void testInList_ListOfStrings() {
        List<String> v1 = Arrays.asList("s1", "s2");
        Constraint c = Constraints.in("p", v1);
        assertNotNull(c);
        assertTrue(c instanceof InConstraint);
        InConstraint ic = (InConstraint) c;
        assertEquals("p", ic.getPropertyName());
        assertArrayEquals(v1.toArray(), ic.getValues().toArray());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testInList_DifferentTypes() {
        List<Object> l = new ArrayList<>();
        l.add(123L);
        l.add("str456");

        Constraint c = Constraints.in("p", l);
        assertNotNull(c);
        assertTrue(c instanceof InConstraint);
        InConstraint ic = (InConstraint) c;
        assertEquals("p", ic.getPropertyName());
        assertArrayEquals(l.toArray(), ic.getValues().toArray());
    }

    @Test
    public void testInIgnoreCase() {
        Object[] v1 = {1, 2};
        Constraint c = Constraints.inIgnoreCase("p", v1);
        assertNotNull(c);
        assertTrue(c instanceof InIgnoreCaseConstraint);
        InIgnoreCaseConstraint ic = (InIgnoreCaseConstraint) c;
        assertEquals("p", ic.getPropertyName());
        assertArrayEquals(v1, ic.getValues().toArray());
    }

    @Test
    public void testInIgnoreCaseList() {
        List<Object> v1 = Arrays.<Object>asList(1, 2);
        Constraint c = Constraints.inIgnoreCase("p", v1);
        assertNotNull(c);
        assertTrue(c instanceof InIgnoreCaseConstraint);
        InIgnoreCaseConstraint ic = (InIgnoreCaseConstraint) c;
        assertEquals("p", ic.getPropertyName());
        assertArrayEquals(v1.toArray(), ic.getValues().toArray());
    }

    @Test
    public void testInIgnoreCaseList_ListOfStrings() {
        List<String> v1 = Arrays.asList("s1", "s2");
        Constraint c = Constraints.inIgnoreCase("p", v1);
        assertNotNull(c);
        assertTrue(c instanceof InConstraint);
        InConstraint ic = (InConstraint) c;
        assertEquals("p", ic.getPropertyName());
        assertArrayEquals(v1.toArray(), ic.getValues().toArray());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testInIgnoreCaseList_DifferentTypes() {
        List<Object> l = new ArrayList<>();
        l.add(123L);
        l.add("str456");

        Constraint c = Constraints.inIgnoreCase("p", l);
        assertNotNull(c);
        assertTrue(c instanceof InConstraint);
        InConstraint ic = (InConstraint) c;
        assertEquals("p", ic.getPropertyName());
        assertArrayEquals(l.toArray(), ic.getValues().toArray());
    }

    @Test
    public void testContains() {
        Constraint c = Constraints.contains("username", "john");
        assertNotNull(c);
        assertTrue(c instanceof ContainsConstraint);
        ContainsConstraint cc = (ContainsConstraint) c;
        assertEquals("username", cc.getPropertyName());
        assertEquals("john", cc.getValue());
    }
}
