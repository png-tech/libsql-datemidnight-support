package net.sf.jparts.libsql.query.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract public class AbstractDecimalConverter<T> implements ValueConverter<T> {

    private Logger logger = LoggerFactory.getLogger(ValueConverter.LOGGER_NAME);

    private final T errorValue;

    private final T nullValue;

    private final boolean safe;

    public AbstractDecimalConverter() {
        this(true, null, null);
    }

    public AbstractDecimalConverter(boolean safe, T errorValue, T nullValue) {
        this.safe = safe;
        this.errorValue = errorValue;
        this.nullValue = nullValue;
    }

    @Override
    public T convert(String value) {
        if (value == null || value.isEmpty()) return nullValue;

        try {
            return tryCreate(value);
        } catch (NumberFormatException ex) {
            if (safe) {
                logger.debug("Can't convert, value = {}", value, ex);
                return errorValue;
            }
            throw new ValueFormatException("Can't convert, value = " + value, ex);
        }
    }

    abstract protected T tryCreate(String value) throws NumberFormatException;
}
