package net.sf.jparts.libsql.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class Configuration {

    private static final Logger logger = LoggerFactory.getLogger(Configuration.class);

    public static Properties get(Class<?> clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("clazz must not be null");
        }

        final String url = "META-INF/libsql/" + clazz.getSimpleName() + ".properties";

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        InputStream is = cl.getResourceAsStream(url);
        if (is == null) {
            logger.debug("Unable to read configuration properties from {}: InputStream is null", url);
            return null;
        }

        try {
            Properties p = new Properties();
            try {
                p.load(is);
                logger.info("Configuration was loaded successfully from {}", url);
                return p;
            } finally {
                is.close();
            }
        } catch (IOException ex) {
            logger.error("Unable to read configuration from {} with exception:\n{}", url, ex.getStackTrace());
            return null;
        }
    }

    public static String getPropertyValue(Properties p, String propertyName, String defaultValue) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("propertyName must not be empty string or null");
        }
        String value = null;
        if (p != null) {
            value = p.getProperty(propertyName.trim());
        }
        if (value == null || value.isEmpty()) {
            value = (defaultValue == null) ? null : defaultValue.trim();
        } else {
            value = value.trim();
        }
        return value;
    }

    public static boolean getBooleanPropertyValue(Properties p, String propertyName, boolean defaultValue) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("propertyName must not be empty string or null");
        }
        String value = null;
        if (p != null) {
            value = p.getProperty(propertyName.trim());
        }
        if (value == null || value.isEmpty()) {
            return defaultValue;
        }
        value = value.toLowerCase();
        if (value.equals("yes") || value.equals("true") || value.equals("on") ||
                value.equals("enable") || value.equals("enabled") || value.equals("1")) {
            return true;
        }
        if (value.equals("no") || value.equals("false") || value.equals("off") ||
                value.equals("disable") || value.equals("disabled") || value.equals("0")) {
            return false;
        }
        return defaultValue;
    }

    private Configuration() {
        // do nothing
    }
}
