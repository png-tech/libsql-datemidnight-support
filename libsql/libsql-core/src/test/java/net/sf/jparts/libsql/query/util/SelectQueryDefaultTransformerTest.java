package net.sf.jparts.libsql.query.util;

import java.util.Map;

import net.sf.jparts.libsql.query.SelectQuery;
import net.sf.jparts.libsql.query.SelectQueryImpl;
import net.sf.jparts.libsql.query.constraints.AbstractJoinConstraint;
import net.sf.jparts.libsql.query.constraints.Constraint;
import net.sf.jparts.libsql.query.constraints.Constraints;
import net.sf.jparts.libsql.query.constraints.Order;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class SelectQueryDefaultTransformerTest {

    private static SelectQueryDefaultTransformer transformer;

    private final static String BASE_QUERY = "select * from users";

    private static SelectQuery<UserEntity> createQuery() {
        return new SelectQueryImpl<>("junit", BASE_QUERY);
    }

    @BeforeClass
    public static void setUpBeforeClass() {
        transformer = new SelectQueryDefaultTransformer("#");
    }

    @AfterClass
    public static void tearDownAfterClass() {
        transformer = null;
    }

    @Test
    public void testConstructor() {
        try {
            new SelectQueryDefaultTransformer(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new SelectQueryDefaultTransformer(" ");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        Object o = new SelectQueryDefaultTransformer("xxx");
        assertNotNull(o);
    }

    @Test
    public void testWithoutParams() {
        TransformationResult r = transformer.buildQueryString(createQuery());
        assertEquals(BASE_QUERY, r.getFullQueryString());
        assertEquals(0, r.getNamedParams().size());
    }

    @Test
    public void testOrder() {
        SelectQuery<UserEntity> sq = createQuery()
                .orderBy(Order.asc("name"), Order.desc("id"));
        final String correct = BASE_QUERY + " order by name asc, id desc";

        TransformationResult r = transformer.buildQueryString(sq);
        assertEquals(correct, r.getFullQueryString());
        assertEquals(0, r.getNamedParams().size());
    }

    @Test
    public void testWhereAndOrder1() {
        SelectQuery<UserEntity> sq = createQuery()
            .where(Constraints.gt("id", 3), Constraints.eq("name", "www"),
                    Constraints.not(Constraints.eq("xxx", "yyy")))
            .orderBy(Order.desc("id").reverse().reverse());
        final String correct = BASE_QUERY + "\n where id > #param_id_1 and name = #param_name_2"
                + " and not (xxx = #param_xxx_3)"
                + " order by id desc";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 3);

        ParameterInfo i = new ParameterInfo("id", "param_id_1");
        Object o = namedParams.get(i);
        assertEquals(3, o);

        i = new ParameterInfo("name", "param_name_2");
        o = namedParams.get(i);
        assertEquals("www", o);

        i = new ParameterInfo("xxx", "param_xxx_3");
        o = namedParams.get(i);
        assertEquals("yyy", o);
    }

    @Test
    public void testWhereBeetween() {
        SelectQuery<UserEntity> sq = createQuery()
            .where(Constraints.between("id", 2, 5));
        final String correct = BASE_QUERY + "\n where id between #param_id_1 and #param_id_2";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 2);

        ParameterInfo i = new ParameterInfo("id", "param_id_1");
        Object o = namedParams.get(i);
        assertEquals(2, o);

        i = new ParameterInfo("id", "param_id_2");
        o = namedParams.get(i);
        assertEquals(5, o);
    }

    @Test
    public void testWhereAnd() {
        SelectQuery<UserEntity> sq = createQuery()
            .where(Constraints.and(Constraints.ge("id", 2), Constraints.le("id", 5)));
        final String correct = BASE_QUERY + "\n where id >= #param_id_1 and id <= #param_id_2";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 2);

        ParameterInfo i = new ParameterInfo("id", "param_id_1");
        Object o = namedParams.get(i);
        assertEquals(2, o);

        i = new ParameterInfo("id", "param_id_2");
        o = namedParams.get(i);
        assertEquals(5, o);
    }

    @Test
    public void testWhereIn() {
        SelectQuery<UserEntity> sq = createQuery()
            .where(Constraints.in("id", 2, 5));
        final String correct = BASE_QUERY + "\n where id in (#param_id_1, #param_id_2)";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 2);

        ParameterInfo i = new ParameterInfo("id", "param_id_1");
        Object o = namedParams.get(i);
        assertEquals(2, o);

        i = new ParameterInfo("id", "param_id_2");
        o = namedParams.get(i);
        assertEquals(5, o);
    }

    @Test
    public void testWhereSqlConstraint() {
        SelectQuery<UserEntity> sq = createQuery()
            .where(Constraints.sql("id = 2"));
        final String correct = BASE_QUERY + "\n where id = 2";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 0);
    }

    @Test
    public void testWhereWithJunctionConstrain() {
        SelectQuery<UserEntity> sq = createQuery()
                .where(Constraints.eq("id", 2), Constraints.disjunction(Constraints.eq("p2", 1), Constraints.eq("p2", 3)));
        final String correct = BASE_QUERY + "\n where id = #param_id_1 and (p2 = #param_p2_2 or p2 = #param_p2_3)";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());
    }

    @Test
    public void testWhereJoinConstraint() {
        SelectQuery<UserEntity> sq = createQuery()
            .where(new TestJoinConstraint(Constraints.ge("id", 2), Constraints.le("id", 5)));
        final String correct = BASE_QUERY + "\n where id >= #param_id_1, id <= #param_id_2";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 2);

        ParameterInfo i = new ParameterInfo("id", "param_id_1");
        Object o = namedParams.get(i);
        assertEquals(2, o);

        i = new ParameterInfo("id", "param_id_2");
        o = namedParams.get(i);
        assertEquals(5, o);
    }

    @Test
    public void testWhereContainsConstraint() {
        SelectQuery<UserEntity> sq = createQuery()
            .where(Constraints.contains("username", "john"));
        final String correct = BASE_QUERY + "\n where upper(username) like upper(#param_username_1)";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 1);

        ParameterInfo i = new ParameterInfo("username", "param_username_1");
        Object o = namedParams.get(i);
        assertEquals("%john%", o);
    }

    @Test
    public void testWhereStartsWithConstraint() {
        SelectQuery<UserEntity> sq = createQuery()
                .where(Constraints.startsWith("username", "john"));
        final String correct = BASE_QUERY + "\n where upper(username) like upper(#param_username_1)";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 1);

        ParameterInfo i = new ParameterInfo("username", "param_username_1");
        Object o = namedParams.get(i);
        assertEquals("john%", o);
    }

    @Test
    public void testWhereStartsWithCaseSensitiveConstraint() {
        SelectQuery<UserEntity> sq = createQuery()
                .where(Constraints.startsWithCs("username", "john"));
        final String correct = BASE_QUERY + "\n where username like #param_username_1";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 1);

        ParameterInfo i = new ParameterInfo("username", "param_username_1");
        Object o = namedParams.get(i);
        assertEquals("john%", o);
    }

    @Test
    public void testWhereEndsWithConstraint() {
        SelectQuery<UserEntity> sq = createQuery()
                .where(Constraints.endsWith("username", "john"));
        final String correct = BASE_QUERY + "\n where upper(username) like upper(#param_username_1)";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 1);

        ParameterInfo i = new ParameterInfo("username", "param_username_1");
        Object o = namedParams.get(i);
        assertEquals("%john", o);
    }

    @Test
    public void testWhereEndsWithCaseSensitiveConstraint() {
        SelectQuery<UserEntity> sq = createQuery()
                .where(Constraints.endsWithCs("username", "john"));
        final String correct = BASE_QUERY + "\n where username like #param_username_1";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 1);

        ParameterInfo i = new ParameterInfo("username", "param_username_1");
        Object o = namedParams.get(i);
        assertEquals("%john", o);
    }

    @Test
    public void testWhereEqualsIgnoreCaseConstraint() {
        SelectQuery<UserEntity> sq = createQuery()
                .where(Constraints.equalsIgnoreCase("username", "john"));
        final String correct = BASE_QUERY + "\n where upper(username) = upper(#param_username_1)";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());

        Map<ParameterInfo, Object> namedParams = r.getNamedParams();
        assertEquals(namedParams.size(), 1);

        ParameterInfo i = new ParameterInfo("username", "param_username_1");
        Object o = namedParams.get(i);
        assertEquals("john", o);
    }

    private final static class TestJoinConstraint extends AbstractJoinConstraint {
        
        TestJoinConstraint(Constraint... constraints) {
            this.setConstraints(constraints);
        }
    }

    @Test
    public void testWhereWithLineBreak() {
        SelectQuery<UserEntity> sq = createQuery()
                .where(Constraints.sql("id = 2"));
        final String correct = BASE_QUERY + "\n where id = 2";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());
    }

    @Test
    public void testWhereWithCustomLineBreak() {
        SelectQuery<UserEntity> sq = createQuery()
                .where(Constraints.sql("id = 2"))
                .setHint(SelectQueryDefaultTransformer.LINE_SEPARATOR_VALUE_HINT, "\r"); // old mac os
        final String correct = BASE_QUERY + "\r where id = 2";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());
    }

    @Test
    public void testWhereWithoutLineBreak() {
        SelectQuery<UserEntity> sq = createQuery()
                .where(Constraints.sql("id = 2"));
        final String correct = BASE_QUERY + "\n where id = 2";
        TransformationResult r = transformer.buildQueryString(sq);

        assertEquals(correct, r.getFullQueryString());
    }

}
