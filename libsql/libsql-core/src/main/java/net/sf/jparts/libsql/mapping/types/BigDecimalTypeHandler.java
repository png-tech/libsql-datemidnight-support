package net.sf.jparts.libsql.mapping.types;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BigDecimalTypeHandler implements TypeHandler<BigDecimal> {

    @Override
    public BigDecimal getResult(ResultSet rs, String column) throws SQLException {
        return rs.getBigDecimal(column);
    }
}
