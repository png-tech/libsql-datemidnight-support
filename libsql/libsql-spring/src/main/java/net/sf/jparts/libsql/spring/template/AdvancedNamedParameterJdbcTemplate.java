package net.sf.jparts.libsql.spring.template;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterUtils;
import org.springframework.jdbc.core.namedparam.ParsedSql;

/**
 * Advanced version of {@code NamedParameterJdbcTemplate}.
 */
public class AdvancedNamedParameterJdbcTemplate extends NamedParameterJdbcTemplate {

    private final CacheFilter cacheFilter;
    private final QueryModifier modifier;

    public static Builder builder(DataSource dataSource) {
        return new Builder(dataSource);
    }

    public static Builder builder(JdbcOperations classicJdbcTemplate) {
        return new Builder(classicJdbcTemplate);
    }

    private AdvancedNamedParameterJdbcTemplate(Builder b) {
        super(b.tpl);
        this.cacheFilter = b.cacheFilter;
        this.modifier = b.modifier;
        this.setCacheLimit(b.cacheLimit);
    }

    @Override
    protected ParsedSql getParsedSql(String sql) {
        String query = (modifier == null) ? sql : modifier.apply(sql);

        if (cacheFilter != null && !cacheFilter.satisfied(query))
            return NamedParameterUtils.parseSqlStatement(query);
        else
            return super.getParsedSql(query);
    }

    public static class Builder {
        private final JdbcOperations tpl;
        private CacheFilter cacheFilter;
        private QueryModifier modifier;
        private int cacheLimit = DEFAULT_CACHE_LIMIT;

        public Builder(DataSource dataSource) {
            this.tpl = new JdbcTemplate(requireNonNull(dataSource, "dataSource must not be null"));
        }

        private Builder(JdbcOperations classicJdbcTemplate) {
            this.tpl = requireNonNull(classicJdbcTemplate, "classicJdbcTemplate must not be null");
        }

        public Builder withCacheFilter(CacheFilter cacheFilter) {
            this.cacheFilter = cacheFilter;
            return this;
        }

        public Builder withModifier(QueryModifier modifier) {
            this.modifier = modifier;
            return this;
        }

        public Builder setCacheLimit(int cacheLimit) {
            if (cacheLimit < 0)
                throw new IllegalArgumentException("cacheLimit must not be negative");
            this.cacheLimit = cacheLimit;
            return this;
        }

        public AdvancedNamedParameterJdbcTemplate build() {
            return new AdvancedNamedParameterJdbcTemplate(this);
        }

        private <T> T requireNonNull(T value, String message) {
            if (value == null)
                throw new IllegalArgumentException(message);
            return value;
        }
    }
}
