package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Operators for use with {@link SimpleConstraint}.
 */
@XmlType
@XmlEnum
public enum Operator {

    /**
     * <code>=</code>, equals.
     */
    @XmlEnumValue("eq")
    EQ,

    /**
     * <code>&lt;&gt;</code>, not equals
     */
    @XmlEnumValue("ne")
    NE,

    /**
     * <code>like</code>.
     */
    @XmlEnumValue("like")
    LIKE,

    /**
     * <code>&gt;</code>, greater than.
     */
    @XmlEnumValue("gt")
    GT,

    /**
     * <code>&lt;</code>, lesser than.
     */
    @XmlEnumValue("lt")
    LT,

    /**
     * <code>&gt;=</code>, greater or equals than.
     */
    @XmlEnumValue("ge")
    GE,

    /**
     * <code>&lt;=</code>, lesser or equals than.
     */
    @XmlEnumValue("le")
    LE

}
