package net.sf.jparts.libsql.mapping;

/**
 * Thrown in case of any failure in {@link ObjectProperty#setValue(Object, Object)}.
 */
public final class UpdatePropertyException extends RuntimeException {

    public UpdatePropertyException(String message, Throwable cause) {
        super(message, cause);
    }
}
