package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DoubleTypeHandler implements TypeHandler<Double> {

    @Override
    public Double getResult(ResultSet rs, String column) throws SQLException {
        double d = rs.getDouble(column);
        return (rs.wasNull()) ? null : d;
    }
}
