package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FloatTypeHandler implements TypeHandler<Float> {

    @Override
    public Float getResult(ResultSet rs, String column) throws SQLException {
        float f = rs.getFloat(column);
        return (rs.wasNull()) ? null : f;
    }
}
