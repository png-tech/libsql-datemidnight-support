package net.sf.jparts.libsql.query.constraints;

import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for {@link AbstractJoinConstraint}.
 */
public class AbstractJoinConstraintTest {
    
    @Test
    public void testToString() {
        AbstractJoinConstraint ac = new AbstractJoinConstraint() {
            private static final long serialVersionUID = -691279166602526649L;
        };
        ac.setConstraints(new TxtConstraint("t1"), new TxtConstraint("t2"), new TxtConstraint("t3"));

        boolean append = ac.isAppendSpaces();
        assertFalse(append);

        String op = ac.getJoinOperator();
        assertEquals(", ", op);

        List<Constraint> constraints = ac.getConstraints();
        assertNotNull(constraints);
        assertEquals(3, constraints.size());

        final String ok = "t1, t2, t3";
        String and = ac.toString();
        assertEquals(ok, and);
    }

}
