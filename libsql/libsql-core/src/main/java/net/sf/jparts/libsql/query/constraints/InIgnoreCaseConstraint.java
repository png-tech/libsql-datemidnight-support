package net.sf.jparts.libsql.query.constraints;

public class InIgnoreCaseConstraint extends InConstraint {

    protected InIgnoreCaseConstraint(String propertyName, Object... values) {
        super(propertyName, values);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("UPPER(" + getPropertyName() + ")");
        sb.append(" in (");
        if (getValues().size() > 0) {
            for (int i = 0; i < getValues().size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("UPPER(").append(getValues().get(i)).append(")");
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
