package net.sf.jparts.libsql.mapping.model.test6;

public class PojoWithPrimitives {

    private boolean f;

    private boolean g;

    private int i = -2;

    private int j;

    private int x = -100;

    public boolean isF() {
        return f;
    }

    public boolean isG() {
        return g;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    public int getX() {
        return x;
    }

    @Override
    public String toString() {
        return "PojoWithPrimitives{" +
                "f=" + f +
                ", g=" + g +
                ", i=" + i +
                ", j=" + j +
                ", x=" + x +
                '}';
    }
}
