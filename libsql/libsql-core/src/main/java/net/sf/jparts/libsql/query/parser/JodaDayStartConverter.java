package net.sf.jparts.libsql.query.parser;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

/**
 * Convert string to Joda DateTime instance with time at start of day.
 */
public class JodaDayStartConverter extends AbstractJodaDateConverter<DateTime> {

    public static final String DEFAULT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    public JodaDayStartConverter() {
        super(DEFAULT_PATTERN);
    }

    public JodaDayStartConverter(String pattern) {
        super(pattern);
    }

    public JodaDayStartConverter(boolean safe, String pattern, DateTime errorValue, DateTime nullValue) {
        super(safe, pattern, errorValue, nullValue);
    }

    @Override
    protected DateTime tryParse(String value, DateTimeFormatter format) {
        return format.parseDateTime(value).withTimeAtStartOfDay();
    }
}
