package net.sf.jparts.libsql.mapping;

public class NestedArgumentMapping implements ArgumentMapping {

    private Class<?> type;

    private String columnPrefix;

    private ResultMapping nested;

    private NestedArgumentMapping() {
        // use Builder
    }

    @Override
    public Class<?> getType() {
        if (type != null) {
            return type;
        }
        if (nested.getType() != null) {
            return nested.getType();
        }
        throw new IllegalStateException("nested argument mapping should have defined type (java class)");
    }

    public ResultMapping getNested() {
        return nested;
    }

    public String getColumnPrefix() {
        return (columnPrefix == null) ? "" : columnPrefix;
    }

    public static class Builder {

        private NestedArgumentMapping result = new NestedArgumentMapping();

        public Builder(String nestedId) {
            result.nested = MappingFactory.getInstance().create(nestedId);
        }

        public Builder(ResultMapping nestedMapping) {
            if (nestedMapping == null) {
                throw new IllegalArgumentException("nestedMapping must not be null");
            }
            result.nested = nestedMapping;
        }

        public Builder setType(Class<?> type) {
            if (type == null) {
                throw new IllegalArgumentException("type must not be null");
            }
            result.type = type;
            return this;
        }

        public Builder setType(String className) {
            if (className == null || className.isEmpty()) {
                throw new IllegalArgumentException("className must not be empty or null");
            }
            try {
                result.type = Class.forName(className);
                return this;
            } catch (ClassNotFoundException ex) {
                throw new IllegalArgumentException(ex.getMessage(), ex);
            }
        }

        public Builder setColumnPrefix(String columnPrefix) {
            if (columnPrefix == null || columnPrefix.isEmpty()) {
                throw new IllegalArgumentException("columnPrefix must not be empty or null");
            }
            result.columnPrefix = columnPrefix;
            return this;
        }

        private void validate() {
            if (result.nested == null) {
                throw new IllegalStateException("nested mapping not defined for argument");
            }
            if (result.type == null) {
                throw new IllegalStateException("argument type is not defined");
            }
        }

        public NestedArgumentMapping build() {
            validate();
            return result;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NestedArgumentMapping that = (NestedArgumentMapping) o;

        if (columnPrefix != null ? !columnPrefix.equals(that.columnPrefix) : that.columnPrefix != null) return false;
        if (nested != null ? !nested.equals(that.nested) : that.nested != null) return false;
        //noinspection RedundantIfStatement
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (columnPrefix != null ? columnPrefix.hashCode() : 0);
        result = 31 * result + (nested != null ? nested.hashCode() : 0);
        return result;
    }
}
