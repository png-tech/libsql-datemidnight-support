package net.sf.jparts.libsql.query.constraints;

public class SimpleConstraint implements OnePropertyConstraint {

    private final String propertyName;

    private final Object value;

    private final String operator;

    protected SimpleConstraint(String propertyName, String operator, Object value) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("propertyName must not be empty string or null");
        }
        if (operator == null || operator.trim().isEmpty()) {
            throw new IllegalArgumentException("operator must not be empty string or null");
        }
        if (value == null) {
            throw new IllegalArgumentException("value must not be null");
        }
        this.propertyName = propertyName.trim();
        this.operator = operator.trim();
        this.value = value;
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }

    public Object getValue() {
        return value;
    }

    public String getOperator() {
        return operator;
    }

    @Override
    public String toString() {
        return propertyName + " " + operator + " " + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleConstraint that = (SimpleConstraint) o;

        if (operator != null ? !operator.equals(that.operator) : that.operator != null) return false;
        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) return false;
        //noinspection RedundantIfStatement
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = propertyName != null ? propertyName.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (operator != null ? operator.hashCode() : 0);
        return result;
    }
}
