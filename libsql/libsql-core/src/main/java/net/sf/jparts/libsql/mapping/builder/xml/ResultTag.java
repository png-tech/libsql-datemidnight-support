package net.sf.jparts.libsql.mapping.builder.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "result")
@XmlType(name = "ResultMapping", propOrder = {
    "constructor",
    "discriminator",
    "properties",
    "associations"
})
public class ResultTag {

    public ConstructorTag constructor;

    public DiscriminatorTag discriminator;

    @XmlElement(name = "property")
    public List<PropertyTag> properties;

    @XmlElement(name = "association")
    public List<AssociationTag> associations;

    @XmlAttribute(name = "id", required = true)
    public String id;

    @XmlAttribute(name = "type", required = true)
    public String type;

    @XmlAttribute(name = "autoMapping")
    public Boolean autoMapping;

}
