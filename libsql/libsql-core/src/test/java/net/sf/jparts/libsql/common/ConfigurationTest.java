package net.sf.jparts.libsql.common;

import net.sf.jparts.libsql.finder.FinderRegistry;
import org.junit.Assert;
import org.junit.Test;

import java.util.Properties;

public class ConfigurationTest {

    @Test
    public void testGet() {
        try {
            Configuration.get(null);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }
        
        Properties p = Configuration.get(ConfigurationTest.class);
        Assert.assertNull(p);

        p = Configuration.get(FinderRegistry.class);
        Assert.assertNotNull(p);
    }

    @Test
    public void testGetPropertyValue() {
        try {
            Configuration.getPropertyValue(null, null, null);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            Configuration.getPropertyValue(null, " ", null);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        String v = Configuration.getPropertyValue(null, "random_property_name", "abc ");
        Assert.assertEquals("abc", v);

        Properties p = new Properties();
        p.setProperty("key", "value");

        v = Configuration.getPropertyValue(p, "key", "abc");
        Assert.assertEquals("value", v);

        v = Configuration.getPropertyValue(p, "random_property_name", "abc");
        Assert.assertEquals("abc", v);
    }

    @Test
    public void testGetBooleanPropertyValue() {
        try {
            Configuration.getBooleanPropertyValue(null, null, false);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            Configuration.getBooleanPropertyValue(null, " ", false);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        boolean b = Configuration.getBooleanPropertyValue(null, "random_property_name", false);
        Assert.assertEquals(false, b);

        Properties p = new Properties();
        p.setProperty("t1", "yEs");
        p.setProperty("t2", "trUe");
        p.setProperty("t3", "oN");
        p.setProperty("t4", "enAble");
        p.setProperty("t5", "enaBled");
        p.setProperty("t6", "1");
        p.setProperty("f1", "NO");
        p.setProperty("f2", "fALSe");
        p.setProperty("f3", "oFF");
        p.setProperty("f4", "disABLe");
        p.setProperty("f5", "dISAbled");
        p.setProperty("f6", "0");
        p.setProperty("xx", "xx");

        // true values
        b = Configuration.getBooleanPropertyValue(p, "t1", false);
        Assert.assertEquals(true, b);
        b = Configuration.getBooleanPropertyValue(p, "t2", false);
        Assert.assertEquals(true, b);
        b = Configuration.getBooleanPropertyValue(p, "t3", false);
        Assert.assertEquals(true, b);
        b = Configuration.getBooleanPropertyValue(p, "t4", false);
        Assert.assertEquals(true, b);
        b = Configuration.getBooleanPropertyValue(p, "t5", false);
        Assert.assertEquals(true, b);
        b = Configuration.getBooleanPropertyValue(p, "t6", false);
        Assert.assertEquals(true, b);

        // false values
        b = Configuration.getBooleanPropertyValue(p, "f1", true);
        Assert.assertEquals(false, b);
        b = Configuration.getBooleanPropertyValue(p, "f2", true);
        Assert.assertEquals(false, b);
        b = Configuration.getBooleanPropertyValue(p, "f3", true);
        Assert.assertEquals(false, b);
        b = Configuration.getBooleanPropertyValue(p, "f4", true);
        Assert.assertEquals(false, b);
        b = Configuration.getBooleanPropertyValue(p, "f5", true);
        Assert.assertEquals(false, b);
        b = Configuration.getBooleanPropertyValue(p, "f6", true);
        Assert.assertEquals(false, b);

        // key exists, but value could not be converted
        b = Configuration.getBooleanPropertyValue(p, "xx", true);
        Assert.assertEquals(true, b);
        b = Configuration.getBooleanPropertyValue(p, "xx", false);
        Assert.assertEquals(false, b);

        b = Configuration.getBooleanPropertyValue(p, "random_property_name", false);
        Assert.assertEquals(false, b);
    }
}
