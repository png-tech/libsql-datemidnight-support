package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class Order implements Serializable {

    private static final long serialVersionUID = -4029849042091671921L;

    @XmlElement(required = false)
    private boolean ascending = true;

    @XmlElement(required = true)
    private String propertyName;

    public Order() {
    }

    public Order(String propertyName) {
        this.propertyName = propertyName;
    }

    public Order(String propertyName, boolean ascending) {
        this.ascending = ascending;
        this.propertyName = propertyName;
    }

    public boolean isAscending() {
        return ascending;
    }

    public void setAscending(boolean ascending) {
        this.ascending = ascending;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (ascending != order.ascending) return false;
        if (propertyName != null ? !propertyName.equals(order.propertyName) : order.propertyName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (ascending ? 1 : 0);
        result = 31 * result + (propertyName != null ? propertyName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Order");
        sb.append("{propertyName='").append(propertyName).append('\'');
        sb.append(", ascending=").append(ascending);
        sb.append('}');
        return sb.toString();
    }
}
