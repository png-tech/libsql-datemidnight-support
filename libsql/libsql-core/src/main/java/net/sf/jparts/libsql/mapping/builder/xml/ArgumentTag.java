package net.sf.jparts.libsql.mapping.builder.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArgumentMapping")
public class ArgumentTag {

    @XmlAttribute(name = "column")
    public String column;

    @XmlAttribute(name = "type")
    public String type;

    @XmlAttribute(name = "typeHandler")
    public String typeHandler;

    @XmlAttribute(name = "result")
    public String result;

    @XmlAttribute(name = "columnPrefix")
    public String columnPrefix;

}
