package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LongTypeHandler implements TypeHandler<Long> {

    @Override
    public Long getResult(ResultSet rs, String column) throws SQLException {
        long l = rs.getLong(column);
        return (rs.wasNull()) ? null : l;
    }
}
