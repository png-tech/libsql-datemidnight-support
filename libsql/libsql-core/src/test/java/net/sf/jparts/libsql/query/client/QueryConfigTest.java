package net.sf.jparts.libsql.query.client;

import java.util.List;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class QueryConfigTest {

    @Test
    public void testGetParameter() throws Exception {
        QueryConfig config = new QueryConfig();
        config.setParameter("param", "v1");
        config.setParameter("param", "v2");

        // first value!
        assertEquals("v1", config.getParameter("param"));
    }

    @Test
    public void testGetParameterValues() throws Exception {
        QueryConfig config = new QueryConfig();
        config.setParameter("param", "v1");
        config.setParameter("param", "v2");

        List<String> values = config.getParameterValues("param");
        assertNotNull(values);
        assertEquals(2, values.size());
        assertEquals("v1", values.get(0));
        assertEquals("v2", values.get(1));
    }

    @Test
    public void testGetParameterValues_ForAbsentParameter() throws Exception {
        QueryConfig config = new QueryConfig();
        assertNull(config.getParameterValues("param"));
    }
}
