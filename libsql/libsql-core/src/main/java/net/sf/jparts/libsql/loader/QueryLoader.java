package net.sf.jparts.libsql.loader;

/**
 * Used to load query string by query name.
 */
public interface QueryLoader {

    /**
     * Loads query string by given query name.
     *
     * @param queryName name of query.
     *
     * @return non-null non-empty string.
     *
     * @throws IllegalArgumentException if <code>queryName</code> is empty or <code>null</code>.
     * @throws QueryLoadException if loaded query is empty or any other runtime error occured.
     */
    public String load(String queryName);
}
