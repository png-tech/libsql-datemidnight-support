package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Implementations are used for retrieving column values of correct java type.
 * @param <T> java type.
 */
public interface TypeHandler<T> {

    /**
     * Get column value from result set.
     * @param rs result set, must not be <code>null</code>.
     * @param column column name, must not be empty or <code>null</code>.
     *
     * @return value of type {@code T} or null.
     *
     * @throws SQLException if an error occurs.
     */
    public T getResult(ResultSet rs, String column) throws SQLException;
}
