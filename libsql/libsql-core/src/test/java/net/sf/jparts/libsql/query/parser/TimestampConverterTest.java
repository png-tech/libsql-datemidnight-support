package net.sf.jparts.libsql.query.parser;

import java.sql.Timestamp;
import java.util.Calendar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TimestampConverterTest {

    @Test
    public void testTimestampConvert() {
        SqlTimestampConverter dc = new SqlTimestampConverter();
        assertNull(dc.convert(null));
        assertNull(dc.convert("str"));
        assertEquals(getTestTimestamp(), dc.convert("2013-12-10T10:10:10"));
    }

    @Test
    public void testTimestampConvertWith() {
        Timestamp current = getTestTimestamp();
        SqlTimestampConverter dc = new SqlTimestampConverter(true, "dd.MM.yyyy", current, current);
        assertEquals(current, dc.convert(null));
        assertEquals(current, dc.convert("str"));
        assertEquals(getTestWithZeroTime(), dc.convert("10.12.2013"));
        assertEquals(current, dc.convert("2013-12-10T10:10:10"));

        dc = new SqlTimestampConverter(true, "dd.MM.yyyy HH:mm:ss", current, current);
        assertEquals(current, dc.convert("10.12.2013 10:10:10"));
    }

    private Timestamp getTestWithZeroTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(2013, Calendar.DECEMBER, 10);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTime().getTime());
    }

    private Timestamp getTestTimestamp() {
        Calendar cal = Calendar.getInstance();
        cal.set(2013, Calendar.DECEMBER, 10);
        cal.set(Calendar.HOUR_OF_DAY, 10);
        cal.set(Calendar.MINUTE, 10);
        cal.set(Calendar.SECOND, 10);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTime().getTime());
    }
}

