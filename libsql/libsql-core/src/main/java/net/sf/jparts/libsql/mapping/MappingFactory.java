package net.sf.jparts.libsql.mapping;

import net.sf.jparts.libsql.mapping.builder.ClassMappingBuilder;
import net.sf.jparts.libsql.mapping.builder.XmlMappingBuilder;

import java.util.HashMap;
import java.util.Map;

public class MappingFactory {

    private Map<String, ResultMapping> cache = new HashMap<>();

    private ClassMappingBuilder javaBuilder = new ClassMappingBuilder();

    private XmlMappingBuilder xmlBuilder = new XmlMappingBuilder();

    private static final MappingFactory instance = new MappingFactory();

    public static MappingFactory getInstance() {
        return instance;
    }

    public synchronized ResultMapping create(Class<?> clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("clazz must not be null");
        }

        String id = "@" + clazz.getName();
        ResultMapping m = cache.get(id);

        if (m == null) {
            m = javaBuilder.build(id, clazz);
            cache.put(id, m);
        }
        return m;
    }

    public synchronized ResultMapping create(String id) {
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException("mapping id must not be empty or null");
        }

        ResultMapping m = cache.get(id);
        if (m == null) {
            if (id.startsWith("@")) {
                try {
                    Class<?> clazz = Class.forName(id.substring(1));
                    m = javaBuilder.build(id, clazz);
                } catch (ClassNotFoundException ex) {
                    throw new IllegalArgumentException("Illegal id specified " + id +
                            ", class not found: " + ex.getMessage(), ex);
                }
            } else {
                m = xmlBuilder.build(id);
            }
            cache.put(m.getId(), m);
        }
        return m;
    }

    private MappingFactory() {
    }
}
