package net.sf.jparts.libsql.query.parser;

import java.sql.Date;
import java.util.Calendar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SqlDateConverterTest {

    @Test
    public void testDateConvert() {
        SqlDateConverter dc = new SqlDateConverter();
        assertNull(dc.convert(null));
        assertNull(dc.convert("str"));
        assertEquals(getTestDateOnly(), dc.convert("2013-12-10"));
    }

    @Test
    public void testDateConvertWith() {
        Date current = new Date(new java.util.Date().getTime());
        SqlDateConverter dc = new SqlDateConverter(true, "dd.MM.yyyy", current, current);
        assertEquals(current, dc.convert(null));
        assertEquals(current, dc.convert("str"));
        assertEquals(getTestDateOnly(), dc.convert("10.12.2013"));
        assertEquals(current, dc.convert("2013-10-10T10:10:10")); // invalid format
    }

    private Date getTestDateOnly() {
        Calendar cal = Calendar.getInstance();
        cal.set(2013, Calendar.DECEMBER, 10);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Date(cal.getTime().getTime());
    }
}
