package net.sf.jparts.libsql.query.constraints;

/**
 * Defines an ordering over the query results.
 */
public class Order {

    /**
     * Creates ascending order.
     * @param propertyName property name.
     * @return a new <code>Order</code> instance.
     * @throws IllegalArgumentException if <code>propertyName</code> is empty string or <code>null</code>.
     */
    public static Order asc(String propertyName) {
        return new Order(propertyName, true);
    }

    /**
     * Creates descending order.
     * @param propertyName property name.
     * @return a new <code>Order</code> instance.
     * @throws IllegalArgumentException if <code>propertyName</code> is empty string or <code>null</code>.
     */
    public static Order desc(String propertyName) {
        return new Order(propertyName, false);
    }

    private boolean ascending = true;

    private String propertyName;

    /**
     * Creates ascending order.
     *
     * @param propertyName property name.
     */
    public Order(String propertyName) {
        this(propertyName, true);
    }

    /**
     * Creates a new <code>Order</code> instance.
     *
     * @param propertyName property name
     * @param ascending pass <code>true</code> for ascending order,
     *      <code>false</code> for descending.
     */
    public Order(String propertyName, boolean ascending) {
        if (propertyName == null || propertyName.trim().length() < 1) {
            throw new IllegalArgumentException("propertyName must not be empty or null");
        }
        this.propertyName = propertyName.trim();
        this.ascending = ascending;
    }

    /**
     * Switch the ordering.
     * @return a new <code>Order</code> instance with the reversed ordering.
     */
    public Order reverse() {
        return new Order(propertyName, ! ascending); 
    }

    /**
     * Whether ascending ordering is in effect.
     * @return boolean indicating whether ordering is ascending.
     */
    public boolean isAscending() {
        return ascending;
    }

    public String getPropertyName() {
        return propertyName;
    }

    @Override
    public String toString() {
        return propertyName + " " + (ascending ? "asc" : "desc");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (ascending != order.ascending) return false;
        //noinspection RedundantIfStatement
        if (!propertyName.equals(order.propertyName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (ascending ? 1 : 0);
        result = 31 * result + propertyName.hashCode();
        return result;
    }
}
