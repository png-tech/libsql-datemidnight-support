package net.sf.jparts.libsql.mapping;

import net.sf.jparts.libsql.mapping.types.TypeHandler;
import net.sf.jparts.libsql.mapping.types.TypeHandlerRegistry;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdvancedResultMapper<T> {

    protected static final ObjectFactory defaultFactory = new DefaultObjectFactory();

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected final ResultMapping root;

    protected ObjectFactory factory;

    protected ConcurrentMap<PropertyKey, ObjectProperty> propertiesCache = new ConcurrentHashMap<>();

    public AdvancedResultMapper(String resultId) {
        this(resultId, defaultFactory);
    }

    public AdvancedResultMapper(String resultId, ObjectFactory factory) {
        checkFactory(factory);
        this.root = MappingFactory.getInstance().create(resultId);
        this.factory = factory;
    }

    public AdvancedResultMapper(ResultMapping mapping) {
        this(mapping, defaultFactory);
    }

    public AdvancedResultMapper(ResultMapping mapping, ObjectFactory factory) {
        if (mapping == null) {
            throw new IllegalArgumentException("root mapping must not be null");
        }
        checkFactory(factory);
        this.root = mapping;
        this.factory = factory;
    }

    public AdvancedResultMapper(Class<T> clazz) {
        this(clazz, defaultFactory);
    }

    public AdvancedResultMapper(Class<T> clazz, ObjectFactory factory) {
        checkFactory(factory);
        this.root = MappingFactory.getInstance().create(clazz);
        this.factory = factory;
    }

    private void checkFactory(ObjectFactory factory) {
        if (factory == null) {
            throw new IllegalArgumentException("factory must not be null");
        }
    }

    @SuppressWarnings("unchecked")
    public T mapRow(ResultSet rs) throws SQLException {
        return (T) createObject(root, "", false, rs);
    }

    @SuppressWarnings("unchecked")
    protected Object createObject(ResultMapping mapping, String columnPrefix, boolean isNested,
            ResultSet rs) throws SQLException {

        DiscriminatorMapping dm = mapping.getDiscriminatorMapping();
        if (dm != null) {
            String value = getColumnValue(columnPrefix + dm.getColumn(), String.class, rs);
            ResultMapping nested = dm.getMappings().get(value);
            if (nested != null) {
                return createObject(nested, columnPrefix, isNested, rs);
            }
        }

        Map<String, Object> propertiesValues = readRow(mapping, columnPrefix, rs);
        boolean allPropertiesAreNull = allNulls(propertiesValues.values().toArray());

        Object result;
        ConstructorMapping cm = mapping.getConstructorMapping();
        if (cm == null || cm.getArguments().isEmpty()) {
            if (isNested && allPropertiesAreNull) {
                result = null;
            } else {
                result = factory.create(mapping.getType());
            }
        } else {
            List<ArgumentMapping> arguments = cm.getArguments();
            Class<?>[] types = new Class<?>[arguments.size()];
            Object[] values = new Object[arguments.size()];
            for (int i = 0; i < arguments.size(); i++) {
                ArgumentMapping a = arguments.get(i);
                types[i] = a.getType();
                values[i] = getArgumentValue(a, columnPrefix, rs);
            }
            result = (allNulls(values) && isNested)
                    ? null
                    : factory.create(mapping.getType(), types, values);
        }

        if (result == null) {
            return null;
        }

        updateResult(mapping, result, propertiesValues);

        return result;
    }

    protected boolean allNulls(Object[] array) {
        if (array == null) {
            throw new IllegalArgumentException("array must not be null");
        }
        if (array.length < 1) {
            return false;
        }
        for (Object o : array) {
            if (o != null) {
                return false;
            }
        }
        return true;
    }

    protected Object getArgumentValue(ArgumentMapping argument, String columnPrefix, ResultSet rs) throws SQLException {
        if (argument instanceof SimpleArgumentMapping) {
            SimpleArgumentMapping a = (SimpleArgumentMapping) argument;
            return getColumnValue(columnPrefix + a.getColumn(), a.getType(), a.getTypeHandler(), rs);
        } else if (argument instanceof NestedArgumentMapping) {
            NestedArgumentMapping a = (NestedArgumentMapping) argument;
            return createObject(a.getNested(), columnPrefix + a.getColumnPrefix(), true, rs);
        }
        throw new IllegalStateException("invalid mapping, argument mapping unsupported: " + argument.getClass());
    }

    @SuppressWarnings("unchecked")
    protected <V> V getColumnValue(String columnName, Class<V> type, ResultSet rs) throws SQLException {
        return (V) getColumnValue(columnName, type, null, rs);
    }

    @SuppressWarnings("unchecked")
    protected Object getColumnValue(String columnName, Class<?> type, TypeHandler<?> typeHandler, ResultSet rs)
            throws SQLException {

        TypeHandler<?> th = typeHandler;
        if (th == null) {
            Class<?> t = type;
            if (t == null) {
                t = Object.class;
            }
            th = TypeHandlerRegistry.getInstance().get(t);
            if (th == null) {
                if (t.isPrimitive()) {
                    return getPrimitiveColumnValue(columnName, type, rs);
                }
                th = TypeHandlerRegistry.getInstance().get(Object.class);
            }
        }
        return th.getResult(rs, columnName);
    }

    protected Object getPrimitiveColumnValue(String columnName, Class<?> type, ResultSet rs) throws SQLException {
        Object value;
        if (boolean.class.equals(type)) {
            value = rs.getBoolean(columnName);
        } else if (byte.class.equals(type)) {
            value = rs.getByte(columnName);
        } else if (double.class.equals(type)) {
            value = rs.getDouble(columnName);
        } else if (float.class.equals(type)) {
            value = rs.getFloat(columnName);
        } else if (int.class.equals(type)) {
            value = rs.getInt(columnName);
        } else if (long.class.equals(type)) {
            value = rs.getLong(columnName);
        } else if (short.class.equals(type)) {
            value = rs.getShort(columnName);
        } else {
            throw new IllegalArgumentException("getPrimitiveColumnValue called with non primitive type. " +
                    "columnName = " + columnName + ", type = " + type.getName());
        }

        return (rs.wasNull()) ? null : value;
    }

    protected Map<String, Object> readRow(ResultMapping mapping, String columnPrefix, ResultSet rs) throws SQLException {
        Map<String, Object> properties = new HashMap<>();

        // return empty map if there is no columns in ResultSet
        ResultSetMetaData meta = rs.getMetaData();
        int columnCount = meta.getColumnCount();
        if (columnCount < 1) {
            return properties;
        }

        // handle auto mapping
        handleAutoMapping(mapping, columnPrefix, rs, properties);
        handleExplicitMapping(mapping, columnPrefix, rs, properties);

        return properties;
    }

    @SuppressWarnings("unchecked")
    protected void handleAutoMapping(ResultMapping mapping, String columnPrefix, ResultSet rs,
            Map<String, Object> properties) throws SQLException {

        if (! mapping.isAutoMapping()) {
            return;
        }

        ResultSetMetaData meta = rs.getMetaData();
        final int columnCount = meta.getColumnCount();

        for (int i = 1; i <= columnCount; i++) {
            // get property name for column
            String column = meta.getColumnName(i); // ??? or getColumnLabel()
            String columnl = column.toLowerCase();
            if (! columnl.startsWith(columnPrefix.toLowerCase())) {
                continue;
            }
            String pname = columnl.substring(columnPrefix.length());

            // skip property if mapped explicitly
            if (mapping.getPropertyMappings().get(pname) != null) {
                continue;
            }

            // find property
            ObjectProperty p = findProperty(mapping.getType(), pname);
            if (p != null) {
                properties.put(p.getName(), getColumnValue(column, p.getType(), rs));
            }
        }
    }

    protected void handleExplicitMapping(ResultMapping mapping, String columnPrefix, ResultSet rs,
            Map<String, Object> properties) throws SQLException {

        for (Map.Entry<String, PropertyMapping> e : mapping.getPropertyMappings().entrySet()) {
            properties.put(e.getKey(), readPropertyValue(mapping, e.getValue(), columnPrefix, rs));
        }
    }

    protected ObjectProperty findProperty(Class<?> type, String propertyName) {
        PropertyKey key = new PropertyKey(type, propertyName);
        ObjectProperty p = propertiesCache.get(key);
        if (p != null) {
            return p;
        }

        p = findFieldProperty(type, propertyName);
        if (p != null) {
            propertiesCache.putIfAbsent(key, p);
        }

        // else {
        // IMPROVE here we can search public setters for property
        //     p = findMethodProperty(type, propertyName);
        //     if (p != null) put in cache
        // }

        return p;
    }

    protected ObjectProperty findFieldProperty(Class<?> type, String propertyName) {
        if (type.isInterface() || type.equals(Object.class)) {
            return null;
        }

        for (Field f : type.getDeclaredFields()) {
            if (f.getName().equalsIgnoreCase(propertyName)) {
                return new FieldProperty(f);
            }
        }

        return findFieldProperty(type.getSuperclass(), propertyName); // findFieldProperty or maybe findProperty ?
    }

    protected Object readPropertyValue(ResultMapping mapping, PropertyMapping pm, String columnPrefix,
            ResultSet rs) throws SQLException {

        if (pm instanceof SimplePropertyMapping) {
            SimplePropertyMapping m = (SimplePropertyMapping) pm;
            ObjectProperty p = findProperty(mapping.getType(), pm.getName());
            Class<?> propertyType = (p == null) ? null : p.getType();
            return getColumnValue(columnPrefix + m.getColumn(), propertyType, m.getTypeHandler(), rs);
        } else if (pm instanceof AssociationMapping) {
            AssociationMapping m = (AssociationMapping) pm;
            return createObject(m.getNested(), columnPrefix + m.getColumnPrefix(), true, rs);
        }
        throw new IllegalStateException("invalid mapping, property mapping unsupported: " + pm.getClass());
    }

    protected void updateResult(ResultMapping mapping, Object result, Map<String, Object> values) {
        for (Map.Entry<String, Object> e : values.entrySet()) {
            ObjectProperty p = findProperty(mapping.getType(), e.getKey());
            if (p == null) {
                logger.warn("Can't find property {} in type {}, mapping id = {}",
                        e.getKey(), mapping.getType(), mapping.getId());
                continue;
            }
            p.setValue(result, e.getValue());
        }
    }

    protected class PropertyKey {

        protected Class<?> type;

        protected String name;

        protected PropertyKey(Class<?> type, String name) {
            this.type = type;
            this.name = name;
        }

        @SuppressWarnings("unchecked")
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PropertyKey that = (PropertyKey) o;

            if (name != null ? !name.equals(that.name) : that.name != null) return false;
            //noinspection RedundantIfStatement
            if (type != null ? !type.equals(that.type) : that.type != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = type != null ? type.hashCode() : 0;
            result = 31 * result + (name != null ? name.hashCode() : 0);
            return result;
        }
    }
}
