package net.sf.jparts.libsql.query.parser;

public final class IntegerConverter extends AbstractIntegerConverter<Integer> {

    public IntegerConverter() {
    }

    public IntegerConverter(boolean safe, Integer errorValue, Integer nullValue, int radix) {
        super(safe, errorValue, nullValue, radix);
    }

    @Override
    protected Integer tryCreate(String value, int radix) throws NumberFormatException {
        return Integer.valueOf(value, radix);
    }
}
