package net.sf.jparts.libsql.finder.spring_ora;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jparts.libsql.finder.Finder;
import net.sf.jparts.libsql.query.SelectQuery;
import net.sf.jparts.libsql.query.constraints.Constraint;
import net.sf.jparts.libsql.query.constraints.InConstraint;
import net.sf.jparts.libsql.query.constraints.InIgnoreCaseConstraint;
import net.sf.jparts.libsql.query.constraints.Order;
import net.sf.jparts.libsql.query.util.ParameterInfo;
import net.sf.jparts.libsql.query.util.SelectQueryDefaultTransformer;
import net.sf.jparts.libsql.query.util.TransformationResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OracleQueryTransformer extends SelectQueryDefaultTransformer {

    public static final String NO_WRAP_FOR_CONSTRAINTS = "--libsql: noWrapForConstraints";

    public static final String TOTAL_LENGTH_COLUMN = "x_total_length";

    public static final String SUB_WHERE = "--libsql: where: ";

    public static final String SUB_WHERE_HINT = "OracleQueryTransformer.subWhere:";

    public static final String SUB_ORDER_BY_HINT = "OracleQueryTransformer.subOrderBy:";

    /** ORA-01795 : Maximum number of expressions in a list is 1000. */
    public static final int MAX_PARAMS_IN_A_LIST = 1000;

    public static final String QUERY_OPTIMIZER = "--libsql: queryOptimizer: ";

    public static final String QUERY_OPTIMIZER_HINT = "OracleQueryTransformer.queryOptimizer:";

    protected Logger logger = LoggerFactory.getLogger(getClass());

    public OracleQueryTransformer() {
        super(":");
    }

    @Override
    public <E> TransformationResult buildQueryString(SelectQuery<E> query) {
        TransformationResult result = defaultTransformation(query);
        processSubConstraints(result, query);
        processOptimizerHints(result, query);
        wrapWithPaging(result, query);
        return result;
    }

    @SuppressWarnings("unchecked")
    protected <E> void processSubConstraints(TransformationResult result, SelectQuery<E> query) {
        Map<String, Object> hints = query.getHints();
        // sub name -> where hint name
        Map<String, String> subs = new HashMap<>();
        for (String h : hints.keySet()) {
            if (h.startsWith(SUB_WHERE_HINT))
                subs.put(h.substring(SUB_WHERE_HINT.length()), h);
        }

        StringBuilder sb = new StringBuilder();
        StringBuilder rb = result.getQueryBuilder();
        for (Map.Entry<String, String> e : subs.entrySet()) {
            String sub = e.getKey();
            String subWhere = e.getValue();

            sb.setLength(0);

            String marker = SUB_WHERE + sub;
            int i = rb.indexOf(marker);
            if (i == -1) {
                logger.warn("SelectQuery contains hint {} but marker {} was not found in query string",
                        subWhere, marker);
                return;
            }

            applyConstraints(sb, result, (List<Constraint>) hints.get(subWhere));

            List<Order> orders = (List<Order>) hints.get(SUB_ORDER_BY_HINT + sub);
            if (orders != null) applyOrderBy(sb, orders);


            i += marker.length();
            rb.insert(i, sb);
        }
    }

    protected <E> void processOptimizerHints(TransformationResult result, SelectQuery<E> query) {
        Map<String, Object> hints = query.getHints();
        Map<String, String> optimizerHints = new HashMap<>();
        for (String h : hints.keySet()) {
            if (h.startsWith(QUERY_OPTIMIZER_HINT))
                optimizerHints.put(h.substring(QUERY_OPTIMIZER_HINT.length()), h);
        }

        StringBuilder sb = new StringBuilder();
        StringBuilder rb = result.getQueryBuilder();
        for (Map.Entry<String, String> e : optimizerHints.entrySet()) {
            String optimizerName = e.getKey();
            String hint = e.getValue();

            sb.setLength(0);

            String marker = QUERY_OPTIMIZER + optimizerName;
            int i = rb.indexOf(marker);
            if (i == -1) {
                logger.warn("SelectQuery contains hint {} but marker {} was not found in query string",
                        hint, marker);
                return;
            }

            sb.append(result.getLineSeparator()).append("/*+ ").append(hints.get(hint)).append(" */");

            i += marker.length();
            rb.insert(i, sb);
        }
    }

    protected <E> void wrapWithPaging(TransformationResult result, SelectQuery<E> query) {
        final boolean noTotalLength = query.getHintValue(Finder.NO_TOTAL_LENGTH_HINT) != null;

        // total length must not be calculated and paging parameters are not set
        if (noTotalLength && query.getFirstResult() == 0 && query.getMaxResults() == Integer.MAX_VALUE) {
            return;
        }

        final String sep = result.getLineSeparator();

        // do not ask db about rows count if hint is present
        final String count = (noTotalLength) ? "" : "count(*) over() " + TOTAL_LENGTH_COLUMN + ",";

        StringBuilder prefix = new StringBuilder(110);
        prefix.append("select x_s.* from (")
                .append(sep)
                .append(" select ").append(count).append(" rownum - 1 as x_rownum, x_t.* from ( ")
                .append(sep);

        //noinspection MismatchedQueryAndUpdateOfStringBuilder
        StringBuilder sb = result.getQueryBuilder();
        sb.insert(0, prefix);
        sb.append(sep)
                .append(" ) x_t").append(sep)
                .append(" ) x_s").append(sep)
                .append(" where x_rownum >= :x_rownum_start and x_rownum < :x_rownum_end");

        Map<ParameterInfo, Object> namedParams = result.getNamedParams();
        namedParams.put(new ParameterInfo("x_rownum_start"), query.getFirstResult());
        namedParams.put(new ParameterInfo("x_rownum_end"), query.getFirstResult() + query.getMaxResults());
    }

    @Override
    protected <E> boolean shouldWrapForConstraints(TransformationResult result, SelectQuery<E> query) {
        return result.getQueryBuilder().indexOf(NO_WRAP_FOR_CONSTRAINTS) == -1;
    }

    @Override
    protected int inConstraintToString(InConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        if (c.getValues().size() <= MAX_PARAMS_IN_A_LIST)
            return super.inConstraintToString(c, j, sb, namedParams);

        int n = j;
        sb.append("(").append(getPropertyName(c)).append(", 1) in (");

        List<Object> values = c.getValues();
        for (int i = 0; i < values.size(); i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append("(");
            sb.append(createNamedParam(c.getPropertyName(), n++, values.get(i), namedParams));
            sb.append(", 1)");
        }

        sb.append(")");
        return n;
    }

    @Override
    protected int inIgnoreCaseConstraintToString(InIgnoreCaseConstraint c, int j, StringBuilder sb,
            Map<ParameterInfo, Object> namedParams) {
        if (c.getValues().size() <= MAX_PARAMS_IN_A_LIST)
            return super.inIgnoreCaseConstraintToString(c, j, sb, namedParams);

        int n = j;
        sb.append("(UPPER(").append(getPropertyName(c)).append("), 1) in (");

        List<Object> values = c.getValues();
        for (int i = 0; i < values.size(); i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append("(");
            sb.append("UPPER(")
                    .append(createNamedParam(c.getPropertyName(), n++, values.get(i), namedParams))
                    .append(")");
            sb.append(", 1)");
        }

        sb.append(")");
        return n;
    }

}
