package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "in")
@XmlAccessorType(XmlAccessType.FIELD)
public class InConstraint implements Constraint {

    private static final long serialVersionUID = 5187428434379197184L;

    @XmlElement(required = true)
    private String property;

    @XmlElementWrapper(name = "values", required = true)
    @XmlElement(name = "value")
    private List<String> values = new ArrayList<>();

    public InConstraint() {
    }

    public InConstraint(String property) {
        this.property = property;
    }

    public InConstraint(String property, List<String> values) {
        this.property = property;
        this.values.addAll(values);
    }

    public InConstraint(String property, String... values) {
        this.property = property;
        Collections.addAll(this.values, values);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public List<String> getValues() {
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InConstraint that = (InConstraint) o;

        if (property != null ? !property.equals(that.property) : that.property != null) return false;
        if (values != null ? !values.equals(that.values) : that.values != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = property != null ? property.hashCode() : 0;
        result = 31 * result + (values != null ? values.hashCode() : 0);
        return result;
    }
}
