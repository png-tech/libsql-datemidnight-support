package net.sf.jparts.libsql.query.client;

import org.junit.Assert;
import net.sf.jparts.libsql.AbstractMarshallTest;
import org.junit.Test;

public class QueryConfigComplexMarshallTest extends AbstractMarshallTest {

    private static final String configXml = 
            "<qc:queryConfig xmlns:qc=\"http://jparts.sf.net/libsql/query/client\">" +
            "<qc:firstResult>113</qc:firstResult>" +
            "<qc:maxResults>452</qc:maxResults>" +
            "<qc:parameters>" +
            "<qc:parameter>" +
            "<qc:name>p1</qc:name>" +
            "<qc:value>v1</qc:value>" +
            "</qc:parameter>" +
            "<qc:parameter>" +
            "<qc:name>p1</qc:name>" +
            "<qc:value>v2</qc:value>" +
            "</qc:parameter>" +
            "</qc:parameters>" +
            "<qc:orders>" +
            "<qc:order>" +
            "<qc:ascending>true</qc:ascending>" +
            "<qc:propertyName>a</qc:propertyName>" +
            "</qc:order>" +
            "<qc:order>" +
            "<qc:ascending>false</qc:ascending>" +
            "<qc:propertyName>b</qc:propertyName>" +
            "</qc:order>" +
            "</qc:orders>" +
            "<qc:constraints>" +
            "<qc:notNull>a</qc:notNull>" +
            "<qc:isNull>b</qc:isNull>" +
            "<qc:not>" +
            "<qc:isNull>c</qc:isNull>" +
            "</qc:not>" +
            "<qc:in>" +
            "<qc:property>d</qc:property>" +
            "<qc:values>" +
            "<qc:value>dv1</qc:value>" +
            "<qc:value>dv2</qc:value>" +
            "<qc:value>dv3</qc:value>" +
            "</qc:values>" +
            "</qc:in>" +
            "<qc:contains>" +
            "<qc:property>e</qc:property>" +
            "<qc:value>substring</qc:value>" +
            "</qc:contains>" +
            "<qc:and>" +
            "<qc:constraints>" +
            "<qc:simple>" +
            "<qc:property>s1</qc:property>" +
            "<qc:op>eq</qc:op>" +
            "<qc:value>sv1</qc:value>" +
            "</qc:simple>" +
            "<qc:simple>" +
            "<qc:property>s2</qc:property>" +
            "<qc:op>ne</qc:op>" +
            "<qc:value>sv2</qc:value>" +
            "</qc:simple>" +
            "<qc:simple>" +
            "<qc:property>s3</qc:property>" +
            "<qc:op>like</qc:op>" +
            "<qc:value>sv3</qc:value>" +
            "</qc:simple>" +
            "</qc:constraints>" +
            "</qc:and>" +
            "<qc:or>" +
            "<qc:constraints>" +
            "<qc:simple>" +
            "<qc:property>s4</qc:property>" +
            "<qc:op>gt</qc:op>" +
            "<qc:value>sv4</qc:value>" +
            "</qc:simple>" +
            "<qc:simple>" +
            "<qc:property>s5</qc:property>" +
            "<qc:op>lt</qc:op>" +
            "<qc:value>sv5</qc:value>" +
            "</qc:simple>" +
            "<qc:simple>" +
            "<qc:property>s6</qc:property>" +
            "<qc:op>ge</qc:op>" +
            "<qc:value>sv6</qc:value>" +
            "</qc:simple>" +
            "<qc:simple>" +
            "<qc:property>s7</qc:property>" +
            "<qc:op>le</qc:op>" +
            "<qc:value>sv7</qc:value>" +
            "</qc:simple>" +
            "</qc:constraints>" +
            "</qc:or>" +

            "<qc:conjunction>" +
            "<qc:constraints>" +
            "<qc:simple>" +
            "<qc:property>x</qc:property><qc:op>eq</qc:op><qc:value>y</qc:value>" +
            "</qc:simple>" +
            "<qc:simple>" +
            "<qc:property>x</qc:property><qc:op>eq</qc:op><qc:value>z</qc:value>" +
            "</qc:simple>" +
            "</qc:constraints>" +
            "</qc:conjunction>" +

            "<qc:disjunction>" +
            "<qc:constraints>" +
            "<qc:simple>" +
            "<qc:property>x</qc:property><qc:op>eq</qc:op><qc:value>y</qc:value>" +
            "</qc:simple>" +
            "<qc:simple>" +
            "<qc:property>x</qc:property><qc:op>eq</qc:op><qc:value>z</qc:value>" +
            "</qc:simple>" +
            "</qc:constraints>" +
            "</qc:disjunction>" +

            "<qc:between>" +
            "<qc:property>b</qc:property>" +
            "<qc:low>l</qc:low>" +
            "<qc:high>h</qc:high>" +
            "</qc:between>" +
            "<qc:startsWith>" +
            "<qc:property>sw</qc:property>" +
            "<qc:value>swv</qc:value>" +
            "</qc:startsWith>" +
            "<qc:startsWithCs>" +
            "<qc:property>sw</qc:property>" +
            "<qc:value>swv</qc:value>" +
            "</qc:startsWithCs>" +
            "<qc:endsWith>" +
            "<qc:property>ew</qc:property>" +
            "<qc:value>ewv</qc:value>" +
            "</qc:endsWith>" +
            "<qc:endsWithCs>" +
            "<qc:property>ew</qc:property>" +
            "<qc:value>ewv</qc:value>" +
            "</qc:endsWithCs>" +

            "<qc:equalsIgnoreCase>" +
            "<qc:property>eic</qc:property>" +
            "<qc:value>eicv</qc:value>" +
            "</qc:equalsIgnoreCase>" +

            "</qc:constraints>" +
            "</qc:queryConfig>";

    @Test
    public void testMarshall() {
        QueryConfig c = new QueryConfig();
        c.setFirstResult(113);
        c.setMaxResults(452);
        c.setParameter("p1", "v1");
        c.setParameter("p1", "v2"); // dublicate params names!
        c.orderBy(new Order("a"), new Order("b", false));
        c.getConstraints().add(new NotNullConstraint("a"));
        c.getConstraints().add(new NullConstraint("b"));
        c.getConstraints().add(new NotConstraint(new NullConstraint("c")));
        c.getConstraints().add(new InConstraint("d", "dv1", "dv2", "dv3"));
        c.getConstraints().add(new ContainsConstraint("e", "substring"));

        AndConstraint ac = new AndConstraint(
            new SimpleConstraint("s1", Operator.EQ, "sv1")
            ,new SimpleConstraint("s2", Operator.NE, "sv2")
            ,new SimpleConstraint("s3", Operator.LIKE, "sv3")
        );
        c.getConstraints().add(ac);

        OrConstraint oc = new OrConstraint(
            new SimpleConstraint("s4", Operator.GT, "sv4")
            ,new SimpleConstraint("s5", Operator.LT, "sv5")
            ,new SimpleConstraint("s6", Operator.GE, "sv6")
            ,new SimpleConstraint("s7", Operator.LE, "sv7")
        );
        c.getConstraints().add(oc);
        c.getConstraints().add(new ConjunctionConstraint(
                new SimpleConstraint("x", Operator.EQ, "y"),
                new SimpleConstraint("x", Operator.EQ, "z")));
        c.getConstraints().add(new DisjunctionConstraint(
                new SimpleConstraint("x", Operator.EQ, "y"),
                new SimpleConstraint("x", Operator.EQ, "z")));

        c.getConstraints().add(new BetweenConstraint("b", "l", "h"));
        c.getConstraints().add(new StartsWithConstraint("sw", "swv"));
        c.getConstraints().add(new StartsWithCaseSensitiveConstraint("sw", "swv"));
        c.getConstraints().add(new EndsWithConstraint("ew", "ewv"));
        c.getConstraints().add(new EndsWithCaseSensitiveConstraint("ew", "ewv"));
        c.getConstraints().add(new EqualsIgnoreCaseConstraint("eic", "eicv"));

        Assert.assertEquals(configXml, marshall(c));
    }

    @Test
    public void testUnmarshall() throws Exception {
        QueryConfig c = unmarshal(configXml, QueryConfig.class);
        Assert.assertEquals(113, c.getFirstResult());
        Assert.assertEquals(452, c.getMaxResults());
        Assert.assertNotNull(c.getParameters());
        Assert.assertEquals(2, c.getParameters().size());
        Assert.assertEquals(new Parameter("p1", "v1"), c.getParameters().get(0));
        Assert.assertEquals(new Parameter("p1", "v2"), c.getParameters().get(1));
        Assert.assertNotNull(c.getOrders());
        Assert.assertEquals(2, c.getOrders().size());
        Assert.assertEquals(new Order("a"), c.getOrders().get(0));
        Assert.assertEquals(new Order("b", false), c.getOrders().get(1));
        Assert.assertNotNull(c.getConstraints());

        Constraint cc = new ConjunctionConstraint(
                new SimpleConstraint("x", Operator.EQ, "y"),
                new SimpleConstraint("x", Operator.EQ, "z"));
        Assert.assertEquals(cc, c.getConstraints().get(7));
        Constraint dc = new DisjunctionConstraint(
                new SimpleConstraint("x", Operator.EQ, "y"),
                new SimpleConstraint("x", Operator.EQ, "z"));
        Assert.assertEquals(dc, c.getConstraints().get(8));

        BetweenConstraint bw = new BetweenConstraint("b", "l", "h");
        Assert.assertEquals(bw, c.getConstraints().get(9));

        StartsWithConstraint sw = new StartsWithConstraint("sw", "swv");
        Assert.assertEquals(sw, c.getConstraints().get(10));

        StartsWithCaseSensitiveConstraint swcs = new StartsWithCaseSensitiveConstraint("sw", "swv");
        Assert.assertEquals(swcs, c.getConstraints().get(11));

        EndsWithConstraint ew = new EndsWithConstraint("ew", "ewv");
        Assert.assertEquals(ew, c.getConstraints().get(12));

        EndsWithCaseSensitiveConstraint ewcs = new EndsWithCaseSensitiveConstraint("ew", "ewv");
        Assert.assertEquals(ewcs, c.getConstraints().get(13));

        EqualsIgnoreCaseConstraint eic = new EqualsIgnoreCaseConstraint("eic", "eicv");
        Assert.assertEquals(eic, c.getConstraints().get(14));

        Assert.assertEquals(15, c.getConstraints().size());
    }
}
