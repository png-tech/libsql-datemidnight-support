package net.sf.jparts.libsql.query.parser;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

public final class JodaDateTimeConverter extends AbstractJodaDateConverter<DateTime> {

    public static final String DEFAULT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    public JodaDateTimeConverter() {
        super(DEFAULT_PATTERN);
    }

    public JodaDateTimeConverter(String pattern) {
        super(pattern);
    }

    public JodaDateTimeConverter(boolean safe, String pattern, DateTime errorValue, DateTime nullValue) {
        super(safe, pattern, errorValue, nullValue);
    }

    @Override
    protected DateTime tryParse(String value, DateTimeFormatter format) {
        return format.parseDateTime(value);
    }
}
