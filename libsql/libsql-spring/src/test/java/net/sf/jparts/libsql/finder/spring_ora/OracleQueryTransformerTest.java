package net.sf.jparts.libsql.finder.spring_ora;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jparts.libsql.query.SelectQuery;
import net.sf.jparts.libsql.query.SelectQueryImpl;
import net.sf.jparts.libsql.query.constraints.Constraints;
import net.sf.jparts.libsql.query.constraints.InConstraint;
import net.sf.jparts.libsql.query.constraints.Order;
import net.sf.jparts.libsql.query.util.ParameterInfo;
import net.sf.jparts.libsql.query.util.TransformationResult;

import org.junit.Test;

import static java.util.Collections.singletonList;
import static net.sf.jparts.libsql.LibSql.makeSelectQueryByString;
import static net.sf.jparts.libsql.finder.Finder.NO_TOTAL_LENGTH_HINT;
import static net.sf.jparts.libsql.finder.spring_ora.OracleQueryTransformer.*;
import static org.junit.Assert.*;

public class OracleQueryTransformerTest {

    private OracleQueryTransformer transformer = new OracleQueryTransformer();

    @Test
    public void testWrapWithPaging_WithoutNewLines() {
        final String original = "original_query";
        final String ok = "select x_s.* from (\n select count(*) over() x_total_length, rownum - 1 as x_rownum, x_t.* " +
                "from ( \n" + original + "\n ) x_t\n ) x_s\n where x_rownum >= :x_rownum_start and x_rownum < :x_rownum_end";

        SelectQueryImpl<Object> q = new SelectQueryImpl<>("_name", "original_query");

        assertEquals(ok, transformer.buildQueryString(q).getFullQueryString());
    }

    @Test
    public void testWrapWithPaging_WithoutNewLines_WithoutPaging() {
        final String original = "original_query";

        SelectQuery<Object> q = new SelectQueryImpl<>("_name", original)
                .setHint(NO_TOTAL_LENGTH_HINT, true);

        assertEquals(original, transformer.buildQueryString(q).getFullQueryString());
    }

    @Test
    public void testShouldWrapForConstraints_QueryWithoutComment() throws Exception {
        TransformationResult result = new TransformationResult("ksdjfnskdjfns");
        assertTrue(transformer.shouldWrapForConstraints(result, null));
    }

    @Test
    public void testShouldWrapForConstraints_QueryWithSpecialComment() throws Exception {
        TransformationResult result = new TransformationResult(
                "ksdjfnskdjfns " + OracleQueryTransformer.NO_WRAP_FOR_CONSTRAINTS);
        assertFalse(transformer.shouldWrapForConstraints(result, null));
    }

    @Test
    public void test_SubConstraints() {
        String subWhereName = "QWE";
        String qsTemplate = "select * from ( select a, b, c from t1 \n" +
                OracleQueryTransformer.SUB_WHERE + subWhereName + "\n )";

        String aValue = "100";
        String cValue = "300";
        String qsResult =
                "select * from ( \n" + // auto wrap
                "select * from ( select a, b, c from t1 \n" +
                OracleQueryTransformer.SUB_WHERE + subWhereName +
                "\n where a = :param_a_2 order by b asc" +
                "\n ) )\n where c = :param_c_1";

        SelectQuery<Object> sq = makeSelectQueryByString(qsTemplate, Object.class)
                .setHint(SUB_WHERE_HINT + subWhereName, singletonList(Constraints.eq("a", aValue)))
                .setHint(SUB_ORDER_BY_HINT + subWhereName, singletonList(Order.asc("b")))
                .setHint(NO_TOTAL_LENGTH_HINT, true)
                .where(Constraints.eq("c", cValue));

        TransformationResult result = transformer.buildQueryString(sq);
        // test query string
        assertEquals(qsResult, result.getFullQueryString());

        // test parameters
        Map<ParameterInfo, Object> namedParams = result.getNamedParams();
        assertEquals(2, namedParams.size());

        ParameterInfo aKey = new ParameterInfo("a", "param_a_2");
        ParameterInfo cKey = new ParameterInfo("c", "param_c_1");
        assertTrue(namedParams.containsKey(aKey));
        assertTrue(namedParams.containsKey(cKey));
        assertEquals(aValue, namedParams.get(aKey));
        assertEquals(cValue, namedParams.get(cKey));
    }

    @Test
    public void test_QueryHints() {
        String hintName = "some_hint";
        String hintValue = "run_faster_please";
        String qsTemplate = "select * from ( select a, b, c from t1 \n" +
                OracleQueryTransformer.QUERY_OPTIMIZER + hintName + "\n )";

        String qsResult = "select * from ( select a, b, c from t1 \n" +
                "--libsql: queryOptimizer: some_hint\n" +
                "/*+ run_faster_please */\n" +
                " )";

        SelectQuery<?> sq = makeSelectQueryByString(qsTemplate, Object.class)
                .setHint(QUERY_OPTIMIZER_HINT + hintName, hintValue)
                .setHint(NO_TOTAL_LENGTH_HINT, true);

        TransformationResult result = transformer.buildQueryString(sq);
        // test query string
        assertEquals(qsResult, result.getFullQueryString());
    }

    @Test
    public void test_ShouldNotRenderSubConstraintsWhenMarkerNotExists() {
        String subWhereName = "QWE";
        String qsTemplate = "select * from ( select a, b, c from t1 )";

        String aValue = "100";
        String cValue = "300";
        String qsResult =
                "select * from ( \n" + // auto wrap
                        "select * from ( select a, b, c from t1 ) )" +
                        "\n where c = :param_c_1";

        SelectQuery<Object> sq = makeSelectQueryByString(qsTemplate, Object.class)
                .setHint(SUB_WHERE_HINT + subWhereName, singletonList(Constraints.eq("a", aValue)))
                .setHint(SUB_ORDER_BY_HINT + subWhereName, singletonList(Order.asc("b")))
                .setHint(NO_TOTAL_LENGTH_HINT, true)
                .where(Constraints.eq("c", cValue));

        TransformationResult result = transformer.buildQueryString(sq);
        // test query string
        assertEquals(qsResult, result.getFullQueryString());

        // test parameters
        Map<ParameterInfo, Object> namedParams = result.getNamedParams();
        assertEquals(1, namedParams.size());

        ParameterInfo cKey = new ParameterInfo("c", "param_c_1");
        assertTrue(namedParams.containsKey(cKey));
        assertEquals(cValue, namedParams.get(cKey));
    }

    @Test
    public void test_InConstraintWithOver9000Params() {
        final String param = "p";
        final int n = 9000;
        assertTrue(n > OracleQueryTransformer.MAX_PARAMS_IN_A_LIST);
        final int j = 123;

        StringBuilder sb = new StringBuilder();
        Map<ParameterInfo, Object> namedParams = new HashMap<>();
        int fj = transformer.inConstraintToString(in(param, n), j, sb, namedParams);

        // check params number
        assertEquals("last param index", j + n, fj);

        // check generated sql
        StringBuilder ok = new StringBuilder();
        ok.append("(").append(param).append(", 1) in (");
        for (int i = 0; i < n; i++) {
            if (i > 0)
                ok.append(", ");
            ok.append("(:param_").append(param).append("_").append(j + i).append(", 1)");
        }
        ok.append(")");
        assertEquals("query part", ok.toString(), sb.toString());

        // check params count in map
        assertEquals(n, namedParams.size());
    }

    @Test
    public void test_InConstraintParamsCountLesserThanMaximumInAList() {
        final String param = "p";
        final int n = OracleQueryTransformer.MAX_PARAMS_IN_A_LIST - 10;
        final int j = 123;

        StringBuilder sb = new StringBuilder();
        Map<ParameterInfo, Object> namedParams = new HashMap<>();
        int fj = transformer.inConstraintToString(in(param, n), j, sb, namedParams);

        // check params number
        assertEquals("last param index", j + n, fj);

        // check generated sql
        StringBuilder ok = new StringBuilder();
        ok.append(param).append(" in (");
        for (int i = 0; i < n; i++) {
            if (i > 0)
                ok.append(", ");
            ok.append(":param_").append(param).append("_").append(j + i).append("");
        }
        ok.append(")");
        assertEquals("query part", ok.toString(), sb.toString());

        // check params count in map
        assertEquals(n, namedParams.size());
    }

    private InConstraint in(String param, int count) {
        List<Object> values = new ArrayList<>(count);
        for (int i = 0; i < count; i++)
            values.add(i);
        return (InConstraint) Constraints.in(param, values);
    }
}
