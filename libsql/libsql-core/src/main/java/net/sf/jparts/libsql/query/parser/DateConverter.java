package net.sf.jparts.libsql.query.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateConverter extends AbstractDateConverter<Date> {

    public static final String DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public DateConverter() {
        super(DEFAULT_FORMAT);
    }

    public DateConverter(String pattern) {
        super(pattern);
    }

    public DateConverter(boolean safe, final String pattern, Date errorValue, Date nullValue) {
        super(safe, pattern, errorValue, nullValue);
    }

    @Override
    protected Date tryParse(String value, SimpleDateFormat format) throws ParseException {
        return format.parse(value);
    }
}
