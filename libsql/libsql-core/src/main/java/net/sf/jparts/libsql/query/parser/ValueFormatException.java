package net.sf.jparts.libsql.query.parser;

/**
 * May be thrown by {@link ValueConverter} implementations to
 * notify caller of invalid input value.
 */
public final class ValueFormatException extends RuntimeException {

    public ValueFormatException(String message) {
        super(message);
    }

    public ValueFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
