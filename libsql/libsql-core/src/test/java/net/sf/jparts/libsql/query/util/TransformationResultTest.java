package net.sf.jparts.libsql.query.util;

import org.junit.Test;

/**
 * Unit test for {@link TransformationResult}.
 */
public class TransformationResultTest {

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_WithNull() {
        new TransformationResult(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_WithEmptyString() {
        new TransformationResult("");
    }

    @Test
    public void testConstructor_WithSpaces() {
        new TransformationResult("    "); // it's ok
    }
}
