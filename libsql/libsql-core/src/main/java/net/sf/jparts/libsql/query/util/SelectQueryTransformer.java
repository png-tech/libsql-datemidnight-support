package net.sf.jparts.libsql.query.util;

import net.sf.jparts.libsql.query.SelectQuery;

public interface SelectQueryTransformer {

    public <E> TransformationResult buildQueryString(SelectQuery<E> query);
}
