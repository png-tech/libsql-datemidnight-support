package net.sf.jparts.libsql.query;

import net.sf.jparts.libsql.query.constraints.Constraint;
import net.sf.jparts.libsql.query.constraints.Order;

import java.util.*;

public class SelectQueryImpl<E> implements SelectQuery<E> {

    private String name;

    private String queryString;

    private int firstResult = 0;

    private int maxResults = Integer.MAX_VALUE;

    private Class<E> resultClass;

    private Map<String, Object> hints = new HashMap<>();

    private Map<QueryParameter<?>, Object> parameters = new HashMap<>();

    private List<Order> orders = new ArrayList<>();

    private List<Constraint> constraints = new ArrayList<>();

    public SelectQueryImpl() {
    }

    public SelectQueryImpl(String name) {
        if (name == null || name.trim().isEmpty()) {
            throw new IllegalArgumentException("name must not be empty or null");
        }
        this.name = name.trim();
    }

    public SelectQueryImpl(String name, String queryString) {
        this(name);
        setQueryString(queryString);
    }

    @Override
    public Class<E> getResultClass() {
        return resultClass;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getQueryString() {
        return queryString;
    }

    @Override
    public int getFirstResult() {
        return firstResult;
    }

    @Override
    public SelectQuery<E> setFirstResult(int firstResult) {
        if (firstResult < 0) {
            throw new IllegalArgumentException("firstResult must be >= 0");
        }
        this.firstResult = firstResult;
        return this;
    }

    @Override
    public int getMaxResults() {
        return maxResults;
    }

    @Override
    public SelectQuery<E> setMaxResults(int maxResults) {
        if (maxResults < 1) {
            throw new IllegalArgumentException("maxResults must be > 0");
        }
        this.maxResults = maxResults;
        return this;
    }

    @Override
    public Map<String, Object> getHints() {
        return new HashMap<>(hints);
    }

    @Override
    public Object getHintValue(String hintName) {
        return hints.get(hintName);
    }

    @Override
    public SelectQuery<E> setHint(String hintName, Object value) {
        if (hintName == null || hintName.trim().isEmpty()) {
            throw new IllegalArgumentException("hintName must not be empty or null");
        }
        hints.put(hintName.trim(), value);
        return this;
    }

    private Class<?> getValueClass(Object value) {
        return (value == null) ? Object.class : value.getClass();
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public SelectQuery<E> setParameter(int position, Object value) {
        parameters.put(new QueryParameterImpl<>(position, getValueClass(value)), value);
        return this;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public SelectQuery<E> setParameter(String name, Object value) {
        parameters.put(new QueryParameterImpl<>(name, getValueClass(value)), value);
        return this;
    }

    @Override
    public Set<QueryParameter<?>> getParameters() {
        return new HashSet<>(parameters.keySet());
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public <T> T getParameterValue(QueryParameter<T> param) {
        return (T) parameters.get(param);
    }

    @Override
    public SelectQuery<E> orderBy(List<Order> orders) {
        return orderBy0(orders);
    }

    @Override
    public SelectQuery<E> orderBy(Order... orders) {
        return orderBy0(Arrays.asList(orders));
    }

    @Override
    public List<Order> getOrderBy() {
        return new ArrayList<>(this.orders);
    }

    @Override
    public SelectQuery<E> where(List<Constraint> constraints) {
        return where0(constraints);
    }

    @Override
    public SelectQuery<E> where(Constraint... constraints) {
        return where0(Arrays.asList(constraints));
    }

    @Override
    public List<Constraint> getConstraints() {
        return new ArrayList<>(constraints);
    }

    private SelectQuery<E> orderBy0(Iterable<Order> i) {
        this.orders.clear();
        if (i != null) {
            for (Order o : i) {
                if (o != null) {
                    this.orders.add(o);
                }
            }
        }
        return this;

    }

    private SelectQuery<E> where0(Iterable<Constraint> i) {
        this.constraints.clear();
        if (i != null) {
            for (Constraint c : i) {
                if (c != null) {
                    this.constraints.add(c);
                }
            }
        }

        return this;
    }

    public void setQueryString(String queryString) {
        if (queryString == null || queryString.trim().isEmpty()) {
            throw new IllegalArgumentException("queryString must not be empty or null");
        }

        this.queryString = queryString.trim();
    }

    public void setResultClass(Class<E> clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("clazz must not be null");
        }
        this.resultClass = clazz;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SelectQueryImpl");
        sb.append("{name='").append(name).append('\'');
        sb.append(", queryString='").append(queryString).append('\'');
        sb.append(", resultClass=").append(resultClass);
        sb.append(", firstResult=").append(firstResult);
        sb.append(", maxResults=").append(maxResults);
        sb.append(", hints=").append(hints);
        sb.append(", parameters=").append(parameters);
        sb.append(", constraints=").append(constraints);
        sb.append(", orders=").append(orders);
        sb.append('}');
        return sb.toString();
    }
}
