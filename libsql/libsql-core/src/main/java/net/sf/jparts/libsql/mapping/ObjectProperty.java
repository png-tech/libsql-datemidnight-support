package net.sf.jparts.libsql.mapping;

/**
 * Encapsulate access to object property.
 */
public interface ObjectProperty {

    /**
     * Return property type (Java class).
     * @return not <code>null</code>.
     */
    public Class<?> getType();

    /**
     * Return property name.
     * @return not <code>null</code>, not empty string.
     */
    public String getName();

    /**
     * Sets property value.
     * @param instance object instance.
     * @param value property value.
     *
     * @throws UpdatePropertyException in case of any problems.
     */
    public void setValue(Object instance, Object value);
}
