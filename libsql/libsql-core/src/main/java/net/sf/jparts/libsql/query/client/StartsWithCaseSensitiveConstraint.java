package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "startsWithCs")
@XmlAccessorType(XmlAccessType.FIELD)
public class StartsWithCaseSensitiveConstraint extends StartsWithConstraint {

    public StartsWithCaseSensitiveConstraint() {
    }

    public StartsWithCaseSensitiveConstraint(String property, String value) {
        super(property, value);
    }
}
