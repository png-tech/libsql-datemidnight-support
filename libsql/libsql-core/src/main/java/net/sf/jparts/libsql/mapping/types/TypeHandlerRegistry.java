package net.sf.jparts.libsql.mapping.types;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Registry of all supported type handlers. Only one handler per Java class supported.
 * <p>
 * Type handlers can be added and removed in runtime. <code>ConcurrentHashMap</code> is used for implementation.
 *
 * @since 2.0
 * @see TypeHandler
 */
public class TypeHandlerRegistry {

    /** Singleton. */
    private static final TypeHandlerRegistry instance = new TypeHandlerRegistry();

    /**
     * Contains all supported type handlers.
     * @see TypeHandler
     */
    private ConcurrentMap<Class<?>, TypeHandler<?>> types = new ConcurrentHashMap<>();

    /**
     * Contains type handlers instances by their class names.
     */
    private ConcurrentMap<String, TypeHandler<?>> cacheByName = new ConcurrentHashMap<>();

    /**
     * Returns current TypeHandlerRegistry instance.
     * @return not <code>null</code>.
     */
    public static TypeHandlerRegistry getInstance() {
        return instance;
    }

    public <T> void add(Class<T> clazz, TypeHandler<T> handler) {
        if (clazz == null) {
            throw new IllegalArgumentException("class must not be null");
        }
        if (handler == null) {
            throw new IllegalArgumentException("handler must not be null");
        }
        types.put(clazz, handler);
        cacheByName.put(handler.getClass().getName(), handler);
    }

    /**
     * Returns type handler for specified class.
     *
     * @param clazz class, supported by returned handler.
     * @return <code>null</code> if specified class is not supported, instance of
     *      {@link TypeHandler} otherwise.
     */
    @SuppressWarnings("unchecked")
    public <T> TypeHandler<T> get(Class<T> clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("class must not be null");
        }
        return (TypeHandler<T>) types.get(clazz);
    }

    /**
     * Returns type handler for specified class name.
     *
     * @param className must not be empty or <code>null</code>.
     * @return <code>null</code> if specified class is not supported, instance of
     *      {@link TypeHandler} otherwise.
     */
    public TypeHandler<?> get(String className) {
        if (className == null || className.isEmpty()) {
            throw new IllegalArgumentException("class must not be empty or null");
        }
        try {
            return types.get(Class.forName(className));
        } catch (ClassNotFoundException ex) {
            throw new IllegalArgumentException("class " + className +
                    " not found; exception = " + ex.getMessage(), ex);
        }
    }

    @SuppressWarnings("unchecked")
    public TypeHandler<?> getByName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("name must not be empty or null");
        }
        TypeHandler<?> th = cacheByName.get(name);
        if (th != null) {
            return th;
        }
        try {
            Class<TypeHandler> clazz = ((Class<TypeHandler>) Class.forName(name));
            th = clazz.newInstance();
            cacheByName.put(name, th);
            return th;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new IllegalArgumentException("TypeHandler can not be created for name = " + name +
                    "; message = " + ex.getMessage(), ex);
        }
    }

    /**
     * Removes type handler for specified class.
     *
     * @param clazz identifies handler to be removed, not <code>null</code>.
     */
    public void remove(Class<?> clazz) {
        TypeHandler<?> th = types.remove(clazz);
        cacheByName.remove(th.getClass().getName());
    }

    /**
     * Default constructor with registry initialization.
     */
    @SuppressWarnings("deprecation")
    private TypeHandlerRegistry() {
        types.put(Object.class, new ObjectTypeHandler());
        types.put(Boolean.class, new BooleanTypeHandler());
        types.put(String.class, new StringTypeHandler());
        types.put(Byte.class, new ByteTypeHandler());
        types.put(Short.class, new ShortTypeHandler());
        types.put(Integer.class, new IntegerTypeHandler());
        types.put(Long.class, new LongTypeHandler());
        types.put(Float.class, new FloatTypeHandler());
        types.put(Double.class, new DoubleTypeHandler());
        types.put(BigDecimal.class, new BigDecimalTypeHandler());
        types.put(BigInteger.class, new BigIntegerTypeHandler());

        types.put(Date.class, new DateTypeHandler());

        // joda time
        types.put(DateTime.class, new DateTimeTypeHandler());
        types.put(DateMidnight.class, new DateMidnightTypeHandler());
    }
}
