package net.sf.jparts.libsql.query.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract public class AbstractDateConverter<T> implements ValueConverter<T> {

    private Logger logger = LoggerFactory.getLogger(ValueConverter.LOGGER_NAME);

    private final T nullValue;

    private final T errorValue;

    private final ThreadLocal<SimpleDateFormat> localFormat;

    private final boolean safe;

    public AbstractDateConverter(String pattern) {
        this(true, pattern, null, null);
    }

    public AbstractDateConverter(boolean safe, final String pattern, T errorValue, T nullValue) {
        this.safe = safe;
        localFormat = new ThreadLocal<SimpleDateFormat>() {
            @Override
            protected SimpleDateFormat initialValue() {
                return new SimpleDateFormat(pattern);
            }
        };
        this.errorValue = errorValue;
        this.nullValue = nullValue;
    }

    @Override
    public T convert(String value) {
        if (value == null || value.isEmpty())
            return nullValue;

        try {
            SimpleDateFormat f = localFormat.get();
            return tryParse(value, f);
        } catch (ParseException ex) {
            if (safe) {
                logger.debug("Can't convert, value = {}", value);
                return errorValue;
            }
            throw new ValueFormatException("Can't convert, value = " + value, ex);
        }
    }

    abstract protected T tryParse(String value, SimpleDateFormat format) throws ParseException;
}
