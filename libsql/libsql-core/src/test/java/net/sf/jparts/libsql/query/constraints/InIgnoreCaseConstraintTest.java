package net.sf.jparts.libsql.query.constraints;

import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for {@link net.sf.jparts.libsql.query.constraints.InIgnoreCaseConstraint}.
 */
public class InIgnoreCaseConstraintTest {

    private static InIgnoreCaseConstraint ic;

    @BeforeClass
    public static void setUpBeforeClass() {
        ic = new InIgnoreCaseConstraint("p", 1, 2, 3);
    }

    @AfterClass
    public static void tearDownAfrterClass() {
        ic = null;
    }

    @Test
    public void testConstructor() {
        try {
            new InIgnoreCaseConstraint(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new InIgnoreCaseConstraint(" ");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new InIgnoreCaseConstraint("p");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            // noinspection RedundantArrayCreation
            new InIgnoreCaseConstraint("p", new Object[]{});
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }
    }
    
    @Test
    public void testGetPropertyName() {
        assertEquals("p", ic.getPropertyName());
    }

    @Test
    public void testGetValues() {
        List<Object> v = ic.getValues();
        assertArrayEquals(v.toArray(), new Object[] {1, 2, 3});
    }

    @Test
    public void testToString() {
        final String ok = "UPPER(p) in (UPPER(1), UPPER(2), UPPER(3))";
        assertEquals(ok, ic.toString());
    }
}
