package net.sf.jparts.libsql.query.constraints;

/**
 * <code>(constraint1 and constraint2 and ... constraintN)</code>
 */
public class ConjunctionConstraint extends AndConstraint {

    public ConjunctionConstraint() {
        group = true;
    }
}
